﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public enum EntityState
    {
        Idle,
        Walking
    }

    [Flags]
    public enum UnitType
    {
        Basic,
        Heavy,
        HeavySlow,
        Fast,
        LightFast,
        FastWait
    }

    public class Unit 
        : Entity
    {
        private UnitAnimationComponent mRenderComponent;
        private UnitPositionComponent mPositionComponent;
        private UnitCollisionComponent mBoundsCollisionComponent;
        private HealthComponent mHealthComponent;
        private UnitHealthRenderComponent mHealthRenderComponent;
        private BuffComponent mBuffComponent;
        private UnitData mData;

        public Unit()
        {
        }

        public override void UnRegister()
        {
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            if (gameLayer == null)
                return;

            //have to explicitly remove the creature from the gamelayer
            //since the creature can reach the end tile when we are in a menu,
            //and the LayerManager.Layer is pointed at the Menu Layer, rather
            //than the Game Layer
            gameLayer.RenderSystem.UnRegister(mRenderComponent);
            gameLayer.RenderSystem.UnRegister(mHealthRenderComponent);
            gameLayer.PositionSystem.UnRegister(mPositionComponent);
            gameLayer.CollisionSystem.UnRegister(mBoundsCollisionComponent);
            gameLayer.TimeSystem.UnRegister(mBuffComponent);
            gameLayer.EntitySystem.UnRegister(this);
        }

        public UnitAnimationComponent RenderComponent
        {
            get { return mRenderComponent; }
            set { mRenderComponent = value; }
        }

        public UnitPositionComponent PositionComponent
        {
            get { return mPositionComponent; }
            set { mPositionComponent = value; }
        }

        public UnitCollisionComponent BoundsCollisionComponent
        {
            get { return mBoundsCollisionComponent;}
            set { mBoundsCollisionComponent = value; }
        }

        public HealthComponent HealthComponent
        {
            get { return mHealthComponent;}
            set { mHealthComponent = value; }
        }

        public UnitHealthRenderComponent HealthRenderComponent
        {
            get { return mHealthRenderComponent;}
            set { mHealthRenderComponent = value; }
        }

        public BuffComponent BuffComponent
        {
            get { return mBuffComponent;}
            set { mBuffComponent = value; }
        }

        public UnitData Data
        {
            get { return mData; }
            set { mData = value; }
        }
    }

    public class UnitBasic : Unit
    {
        public UnitBasic(int x, int y, TileCoord endTileCoords, string texturePath, Rectangle sourceRectangle, int speed, UnitData unitData)
        {
            RenderComponent = new UnitAnimationComponent(texturePath, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 4, 2, this);
            PositionComponent = new UnitPositionComponent(x, y, endTileCoords, speed, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new UnitCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            HealthComponent = new HealthComponent(unitData.Health, this);
            HealthRenderComponent = new UnitHealthRenderComponent("healthbarborder", "healthbarfill", this);
            BuffComponent = new BuffComponent(this);
            Data = unitData;
        }
    }

    public class UnitHeavy : Unit
    {
        public UnitHeavy(int x, int y, TileCoord endTileCoords, string texturePath, Rectangle sourceRectangle, int speed, UnitData unitData)
        {
            RenderComponent = new UnitAnimationComponent(texturePath, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 4, 2, this);
            PositionComponent = new UnitPositionComponent(x, y, endTileCoords, speed, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new UnitCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            HealthComponent = new HealthComponent(unitData.Health, this);
            HealthRenderComponent = new UnitHealthRenderComponent("healthbarborder", "healthbarfill", this);
            BuffComponent = new BuffComponent(this);
            Data = unitData;
        }
    }

    public class UnitHeavySlow : Unit
    {
        public UnitHeavySlow(int x, int y, TileCoord endTileCoords, string texturePath, Rectangle sourceRectangle, int speed, UnitData unitData)
        {
            RenderComponent = new UnitAnimationComponent(texturePath, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 4, 2, this);
            PositionComponent = new UnitPositionComponent(x, y, endTileCoords, speed, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new UnitCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            HealthComponent = new HealthComponent(unitData.Health, this);
            HealthRenderComponent = new UnitHealthRenderComponent("healthbarborder", "healthbarfill", this);
            BuffComponent = new BuffComponent(this);
            Data = unitData;
        }
    }

    public class UnitLightFast : Unit
    {
        public UnitLightFast(int x, int y, TileCoord endTileCoords, string texturePath, Rectangle sourceRectangle, int speed, UnitData unitData)
        {
            RenderComponent = new UnitAnimationComponent(texturePath, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 4, 2, this);
            PositionComponent = new UnitPositionComponent(x, y, endTileCoords, speed, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new UnitCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            HealthComponent = new HealthComponent(unitData.Health, this);
            HealthRenderComponent = new UnitHealthRenderComponent("healthbarborder", "healthbarfill", this);
            BuffComponent = new BuffComponent(this);
            Data = unitData;
        }
    }

    public class UnitFast : Unit
    {
        public UnitFast(int x, int y, TileCoord endTileCoords, string texturePath, Rectangle sourceRectangle, int speed, UnitData unitData)
        {
            RenderComponent = new UnitAnimationComponent(texturePath, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 4, 2, this);
            PositionComponent = new UnitPositionComponent(x, y, endTileCoords, speed, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new UnitCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            HealthComponent = new HealthComponent(unitData.Health, this);
            HealthRenderComponent = new UnitHealthRenderComponent("healthbarborder", "healthbarfill", this);
            BuffComponent = new BuffComponent(this);
            Data = unitData;
        }
    }

    public class UnitFastWait : Unit
    {
        public UnitFastWait(int x, int y, TileCoord endTileCoords, string texturePath, Rectangle sourceRectangle, int speed, UnitData unitData)
        {
            RenderComponent = new UnitAnimationComponent(texturePath, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 4, 2, this);
            PositionComponent = new UnitFastWaitPositionComponent(x, y, endTileCoords, speed, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new UnitCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            HealthComponent = new HealthComponent(unitData.Health, this);
            HealthRenderComponent = new UnitHealthRenderComponent("healthbarborder", "healthbarfill", this);
            BuffComponent = new BuffComponent(this);
            Data = unitData;
        }
    }
}
