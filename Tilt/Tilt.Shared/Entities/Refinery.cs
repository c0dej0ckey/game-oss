﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public class Refinery 
        : Entity
        , IPlaceable
        , IBuildable
        , ICollideable
        , IHealable
        , IShieldable
    {
        private PositionComponent mPositionComponent;
        private RefineryRenderComponent mRenderComponent;
        private BoundsCollisionComponent mBoundsCollisionComponent;
        private RefineryHarvestComponent mHarvestComponent;
        private HealthComponent mHealthComponent;
        private HealthRenderComponent mHealthRenderComponent;
        private ShieldRenderComponent mShieldRenderComponent;
        private LoopingAudioComponent mAudioComponent;
        private IData mData;
        private bool mIsEnabled;

        public Refinery(string texturePath, int x, int y, Rectangle sourceRectangle, IData data)
        {
            mRenderComponent = new RefineryRenderComponent(texturePath, sourceRectangle, this);
            mPositionComponent = new PositionComponent(x,y,this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            mBoundsCollisionComponent = new BoundsCollisionComponent(
                new Rectangle(x,y, TileMap.TileWidth, TileMap.TileHeight), this);
            mHarvestComponent = new RefineryHarvestComponent(this);
            mHealthComponent = new ResistantHealthComponent((data as RefineryData).Health, this);
            mShieldRenderComponent = new ShieldRenderComponent("lazerbase1", this);
            mAudioComponent = new LoopingAudioComponent("Powerup2", this);
            mData = data;
        }

        public override void UnRegister()
        {
            mRenderComponent.UnRegister();
            mPositionComponent.UnRegister();
            mBoundsCollisionComponent.UnRegister();
            mHarvestComponent.UnRegister();
            mHealthComponent.UnRegister();
            mShieldRenderComponent.UnRegister();
            mAudioComponent.UnRegister();
            mData = null;
            base.UnRegister();
        }

        public ObjectType ObjectType { get { return ObjectType.Refinery; } }

        public IData Data
        {
            get { return mData; }
            set { mData = value; }
                
        }

        public PositionComponent PositionComponent
        {
            get { return mPositionComponent; }
            set { mPositionComponent = value; }
        }

        public RefineryRenderComponent RenderComponent
        {
            get { return mRenderComponent; }
            set { mRenderComponent = value; }
        }

        public BoundsCollisionComponent BoundsCollisionComponent
        {
            get { return mBoundsCollisionComponent; }
            set { mBoundsCollisionComponent = value; }
        }

        public RefineryHarvestComponent HarvestComponent
        {
            get { return mHarvestComponent;}
            set { mHarvestComponent = value; }
        }

        public HealthComponent HealthComponent
        {
            get { return mHealthComponent; }
            set { mHealthComponent = value; }
        }

        public HealthRenderComponent HealthRenderComponent
        {
            get { return mHealthRenderComponent; }
            set { mHealthRenderComponent = value; }
        }

        public ShieldRenderComponent ShieldRenderComponent
        {
            get { return mShieldRenderComponent; }
            set { mShieldRenderComponent = value; }
        }

        public LoopingAudioComponent AudioComponent
        {
            get { return mAudioComponent;}
            set { mAudioComponent = value; }
        }

        public bool Enabled 
        {
            get { return mIsEnabled; }
            set { mIsEnabled = value; } 
        }

    }

    public class RefineryRenderComponent : RenderComponent
    {
        private Rectangle mSourceRectangle;
        public RefineryRenderComponent(string texturePath, Rectangle sourceRectangle, Entity owner, bool register = true) : base(texturePath, owner, register)
        {
            mSourceRectangle = sourceRectangle;
        }

        public override void Update()
        {
            Refinery refinery = Owner as Refinery;
            PositionComponent positionComponent = refinery.PositionComponent;

            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();

            spriteBatch.Draw(mTexture, new Vector2(positionComponent.X, positionComponent.Y), mSourceRectangle, Color.White,
                0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.3f);

        }
    }
}
