﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public class ShieldGenerator 
        : Entity
        , IPlaceable
        , ICollideable
        , IBuildable
        , IHealable
    {
        private IData mData;
        private bool mIsEnabled;

        private ShieldGeneratorAnimationComponent mAnimationComponent;
        private PositionComponent mPositionComponent;
        private ShieldGeneratorAddOnComponent mAddOnComponent;
        private BoundsCollisionComponent mBoundsCollisionComponent;
        private HealthComponent mHealthComponent;
        private HealthRenderComponent mHealthRenderComponent;

        public ShieldGenerator(string texturePath, int x, int y, Rectangle sourceRectangle, float interval, int rows, int columns, IData data)
        {
            mAnimationComponent = new ShieldGeneratorAnimationComponent(texturePath, sourceRectangle, interval, rows, columns, this);
            mPositionComponent = new PositionComponent(x,y,this, new Vector2(x+ TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            mBoundsCollisionComponent = new ShieldGeneratorCollisionComponent(new Rectangle(x,y,TileMap.TileWidth, TileMap.TileHeight), this);
            mHealthComponent = new ResistantHealthComponent((data as ShieldGeneratorData).Health, this);
            mAddOnComponent = new ShieldGeneratorAddOnComponent((data as ShieldGeneratorData).FieldOfView, this);
            mData = data;
        }

        public override void UnRegister()
        {
            mAnimationComponent.UnRegister();
            mPositionComponent.UnRegister();
            mBoundsCollisionComponent.UnRegister();
            mHealthComponent.UnRegister();
            mAddOnComponent.UnRegister();
            mData = null;
            base.UnRegister();
        }


        public ObjectType ObjectType { get { return ObjectType.ShieldGenerator; } }
        public IData Data { get { return mData; } set { mData = value; } }
        public PositionComponent PositionComponent 
        { 
            get {return mPositionComponent; } 
            set { mPositionComponent = value;} 
        }

        public BoundsCollisionComponent BoundsCollisionComponent
        {
            get {return mBoundsCollisionComponent;}
            set { mBoundsCollisionComponent = value; }
        }

        public HealthComponent HealthComponent
        {
            get { return mHealthComponent; }
            set { mHealthComponent = value; }
        }

        public HealthRenderComponent HealthRenderComponent
        {
            get { return mHealthRenderComponent; }
            set { mHealthRenderComponent = value; }
        }

        public bool Enabled
        {
            get { return mIsEnabled; }
            set { mIsEnabled = value; }
        }
    }

    public class ShieldGeneratorAnimationComponent : AnimationComponent
    {
        private Effect mTransparentEffect;

        public ShieldGeneratorAnimationComponent(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner) 
            : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
            //mTransparentEffect = AssetOps.LoadAsset<Effect>("TransparentEffect");
        }

        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            ShieldGenerator shieldGenerator = Owner as ShieldGenerator;
            PositionComponent positionComponent = shieldGenerator.PositionComponent;

            TileNode tile = TileMap.GetTileForPosition(positionComponent.X, positionComponent.Y);
            if (tile != null && tile.Type == TileType.Placed)
            {
                Layer layer = LayerManager.GetLayerOfEntity(shieldGenerator);

              //  spriteBatch.End();

              //  spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);
              //  mTransparentEffect.Parameters["Grayscale"].SetValue(true);
              //  mTransparentEffect.CurrentTechnique.Passes[0].Apply();
                spriteBatch.Draw(mTexture, new Vector2(positionComponent.X, positionComponent.Y), CurrentRectangle, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.3f);
              //  spriteBatch.End();

               // spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);
            }
            else
                spriteBatch.Draw(mTexture, positionComponent.Position, CurrentRectangle, Color.White, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.3f);

            if (SystemsManager.Instance.IsPaused)
                return;

            CurrentTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(CurrentTime <= 0.0f)
            {
                CurrentTime = Interval;
                CurrentColumnIndex++;
            }

            if(CurrentColumnIndex >= Columns)
            {
                CurrentColumnIndex = 0;
            }

            CurrentRectangle = new Rectangle(CurrentColumnIndex * SourceRectangle.Width, CurrentRowIndex * SourceRectangle.Height, SourceRectangle.Width, SourceRectangle.Height);
            
        }
    }

    public class ShieldGeneratorAddOnComponent : EventComponent
    {
        private float mFieldOfView;
        public ShieldGeneratorAddOnComponent(float fieldOfView, Entity owner, bool register = true) : base(owner, register)
        {
            mFieldOfView = fieldOfView;
        }

        public override void Register()
        {
            EventSystem.SubScribe(EventType.TowerAdded, OnTowerAdded_);
            base.Register();
        }

        public override void UnRegister()
        {
            EventSystem.UnSubScribe(EventType.TowerAdded, OnTowerAdded_);
            QueryTowers_(false);
            base.UnRegister();
        }


        private void QueryTowers_(bool addIncrease)
        {
            ShieldGenerator addOn = Owner as ShieldGenerator;
            CollisionComponent collisionComponent = addOn.BoundsCollisionComponent;
            PositionComponent positionComponent = addOn.PositionComponent;

            if (collisionComponent.Cells == null ||
                collisionComponent.Cells.Count == 0)
                return;

            List<int> surroundingCells = CollisionHelper.GetSurroundingCells(collisionComponent.Cells.First());
            foreach (int cell in surroundingCells)
            {
                List<CollisionComponent> nearbyComponents = CollisionHelper.GetNearby(cell);
                foreach (CollisionComponent component in nearbyComponents)
                {
                    if (!(component.Owner is IShieldable))
                        continue;

                    IShieldable shieldable = component.Owner as IShieldable;
                    IPlaceable placeable = component.Owner as IPlaceable;

                    if (shieldable == null || placeable == null)
                        return;

                    if (Vector2.Distance(positionComponent.Origin, placeable.PositionComponent.Origin) < mFieldOfView)
                    {
                        shieldable.ShieldRenderComponent.IsEnabled = addIncrease;
                    }
                }
            }
        }

        private void OnTowerAdded_(object sender, IGameEventArgs e)
        {
            ShieldGenerator sg = Owner as ShieldGenerator;
            PositionComponent positionComponent = sg.PositionComponent;
            CollisionComponent collisionComponent = sg.BoundsCollisionComponent;
            Vector2 origin = positionComponent.Origin;

            if (sender is List<IPlaceable>)
            {
                List<IPlaceable> objects = sender as List<IPlaceable>;
                if (objects.Any(o => o == Owner))
                {
                    QueryTowers_(true);
                }
                else
                {
                    foreach (IPlaceable obj in objects)
                    {
                        if (Vector2.Distance(origin, obj.PositionComponent.Origin) < mFieldOfView && obj is Tower)
                        {
                            IShieldable shieldable = obj as IShieldable;
                            shieldable.ShieldRenderComponent.IsEnabled = true;
                        }
                    }
                }

            }
            else
            {
                IPlaceable placeable = sender as IPlaceable;
                if (Vector2.Distance(origin, placeable.PositionComponent.Origin) < mFieldOfView && placeable is Tower)
                {
                    IShieldable shieldable = placeable as IShieldable;
                    shieldable.ShieldRenderComponent.IsEnabled = true;
                }
            }


        }
    }

    public class ShieldGeneratorCollisionComponent : BoundsCollisionComponent
    {
        public ShieldGeneratorCollisionComponent(Rectangle bounds, Entity owner) : base(bounds, owner)
        {
        }

        public override void Update()
        {
            ShieldGenerator shieldGenerator = Owner as ShieldGenerator;
            PositionComponent positionComponent = shieldGenerator.PositionComponent;

            TileNode tile = TileMap.GetTileForPosition(positionComponent.X, positionComponent.Y);

            if (tile.IsTowerPlaced)
                return;

            foreach (int cell in Cells)
            {
                List<CollisionComponent> nearbyComponents = CollisionHelper.GetNearby(cell);

                foreach (CollisionComponent component in nearbyComponents)
                {
                    if (!(component.Owner is Unit))
                        continue;

                    Unit unit = component.Owner as Unit;
                    UnitPositionComponent unitPosition = unit.PositionComponent;
                    UnitData unitData = unit.Data;

                    HealthComponent healthComponent = shieldGenerator.HealthComponent;

                    if (Vector2.Distance(positionComponent.Position, unitPosition.Position) < TileMap.TileWidth)
                    {
                        unit.UnRegister();
                        EventSystem.EnqueueEvent(EventType.UnitDestroyed, unit, null);

                        healthComponent.Health -= unit.Data.Damage;
                    }

                    if (healthComponent.Health <= 0)
                    {
                        EventSystem.EnqueueEvent(EventType.TowerDestroyed, Owner, null);
                    }
                }
            }
        }
    }

    public class ShieldRenderComponent : RenderComponent
    {
        private bool mIsEnabled;
        private bool mPreviousIsEnabled;

        public ShieldRenderComponent(string texturePath, Entity owner, bool register = true) : base(texturePath, owner, register)
        {
        }

        public bool IsEnabled
        {
            get { return mIsEnabled; }
            set { mIsEnabled = value; }
        }

        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            PositionComponent positionComponent = (Owner as IPlaceable).PositionComponent;

            if (mIsEnabled)
            {
                spriteBatch.Draw(mTexture, positionComponent.Position, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.7f);
            }
        }
    }
}
