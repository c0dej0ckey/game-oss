﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Components;

namespace Tilt.EntityComponent.Entities
{
    public class Camera : Entity
    {
        private CameraPositionComponent mPositionComponent;
#if WIN32
        private CameraMouseComponent mMouseComponent;
        private CameraKeyboardComponent mKeyboardComponent;
#endif

        private CameraTouchComponent mTouchComponent;
 
        public Camera(int x, int y, Viewport viewport)
        {
            mPositionComponent = new CameraPositionComponent(x,y, viewport, this);

            
#if WIN32
            mMouseComponent = new CameraMouseComponent(this);

            mKeyboardComponent = new CameraKeyboardComponent(this);
#endif

            mTouchComponent = new CameraTouchComponent(this);
        }

        public override void UnRegister()
        {
            mPositionComponent.UnRegister();
//#if !WIN32
            mTouchComponent.UnRegister();
//#endif
            base.UnRegister();
        }


        public CameraPositionComponent PositionComponent
        {
            get { return mPositionComponent; }
            set { mPositionComponent = value; }
        }
#if WIN32
        public CameraMouseComponent MouseComponent
        {
            get { return mMouseComponent; }
            set { mMouseComponent = value; }
        }
#endif

//#if !WIN32
        public CameraTouchComponent TouchComponent
        {
            get { return mTouchComponent; }
            set { mTouchComponent = value; }
        }
//#endif


    }
}
