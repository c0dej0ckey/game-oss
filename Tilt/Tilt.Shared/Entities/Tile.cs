﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    [System.Diagnostics.DebuggerDisplay("X: {X} Y: {Y}")]
    public class TileCoord
    { 

        public int X { get; set; }
        public int Y { get; set; }

    }

    public enum ResourceType
    {
        Gold
    }

    public class ResourceTile : TileCoord
    {
        public ResourceType ResourceType { get; set; }

        public int Amount { get; set; }
    }

    public class Tile : Entity
    {
        private PositionComponent mPositionComponent;
        private TileRenderComponent mRenderComponent;

        public Tile(Rectangle bounds) : base()
        {
            mPositionComponent = new PositionComponent(bounds.X, bounds.Y, this);
           // mTouchComponent = new TouchAreaComponent(bounds, this);
           // mRenderComponent = new TileRenderComponent("occupied", "empty", this);
        }

        public PositionComponent PositionComponent
        {
            get { return mPositionComponent; }
        }

        public TileRenderComponent RenderComponent
        {
            get { return mRenderComponent;}
            set { mRenderComponent = value; }
        }

        public override void UnRegister()
        {
            mPositionComponent.UnRegister();
            base.UnRegister();
        }
    }
}
