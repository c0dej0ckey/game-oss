﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public class ResourcePile 
        : Entity
        , IPlaceable
    {
        private IData mData;
        private PositionComponent mPositionComponent;

        public ObjectType ObjectType { get {return ObjectType.ResourcePile; } }

        public IData Data
        {
            get { return mData; }
            set { mData = value; }
        }
        public PositionComponent PositionComponent
        {
            get {return mPositionComponent;}
            set { mPositionComponent = value; }
        }

        public override void UnRegister()
        {
            mPositionComponent.UnRegister();
            mData = null;
        }
    }

    public class ResourceRenderComponent : RenderComponent
    {
        public ResourceRenderComponent(string texturePath, Entity owner, bool register = true) : base(texturePath, owner, register)
        {
        }

        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GoldPile goldPile = Owner as GoldPile;
            PositionComponent positionComponent = goldPile.PositionComponent;

            spriteBatch.Draw(mTexture, positionComponent.Position, null, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.2f);
        }
    }

    public class GoldPile : ResourcePile
    {
        private ResourceRenderComponent mResourceRenderComponent;

        //parse from json
        //dead tile in tilemap
        //make sure no building can be placed on
        //resource loader has to be placed next to it

        public GoldPile(string texturePath, int x, int y, IData pileData)
        {
            mResourceRenderComponent = new ResourceRenderComponent(texturePath, this);
            PositionComponent = new PositionComponent(x,y, this);
            Data = pileData;
        }

        public override void UnRegister()
        {
            mResourceRenderComponent.UnRegister();
        }
    }
}
