﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public enum ProjectileType
    {
        Bullet,
        Laser,
        Fire,
        Heavy,
        Shotgun,
        Sludge,
        Rocket
    }

    public class Projectile : Entity
    {

        private ProjectileType mProjectileType;
        private PositionComponent mPositionComponent;
        private CollisionComponent mCollisionComponent;
        private ProjectileData mData;

        public Projectile(ProjectileType projectileType)
        {
            mProjectileType = projectileType;

        }

        public ProjectileType ProjectileType
        {
            get { return mProjectileType;}
        }

        public PositionComponent PositionComponent
        {
            get { return mPositionComponent; }
            set { mPositionComponent = value; }
        }

        public CollisionComponent CollisionComponent
        {
            get { return mCollisionComponent; }
            set { mCollisionComponent = value; }
        }

        public ProjectileData Data
        {
            get { return mData; }
            set { mData = value; }
        }
    }

    public class Bullet : Projectile
    {
        private BulletRenderComponent mRenderComponent;
        public Bullet(string texturePath, int x, int y, Rectangle sourceRectangle, float rotation, ProjectileData projectileData) : base(ProjectileType.Bullet)
        {
            PositionComponent = new BulletPositionComponent(x,y, rotation, this);
            CollisionComponent = new BoundsCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            RenderComponent = new BulletRenderComponent(texturePath, sourceRectangle, this);
            Data = projectileData;
        }

        public BulletRenderComponent RenderComponent
        {
            get { return mRenderComponent;}
            set { mRenderComponent = value; }
        }

        public override void UnRegister()
        {
            PositionComponent.UnRegister();
            CollisionComponent.UnRegister();
            RenderComponent.UnRegister();
            base.UnRegister();
        }
    }
}
