﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public class RefineryAddOn : AddOn
    {
        private RefineryAddOnAnimationComponent mRenderComponent;

        public RefineryAddOn(string texturePath, int x, int y, Rectangle sourceRectangle, float interval, int rows, int columns, IData data) : base(AddOnType.Refinery)
        {
            PositionComponent = new PositionComponent(x, y, this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            BoundsCollisionComponent = new AddOnCollisionComponent(new Rectangle(x, y, TileMap.TileWidth, TileMap.TileHeight), this);
            mRenderComponent = new RefineryAddOnAnimationComponent(texturePath, sourceRectangle, interval, rows, columns, this);
            HealthComponent = new ResistantHealthComponent((data as AddOnData).Health, this);
            ShieldRenderComponent = new ShieldRenderComponent("lazerbase1", this);
            Data = data;
        }

        public override void UnRegister()
        {
            mRenderComponent.UnRegister();
            PositionComponent.UnRegister();
            BoundsCollisionComponent.UnRegister();
            HealthComponent.UnRegister();
            ShieldRenderComponent.UnRegister();
            Data = null;

            base.UnRegister();
        }
    }

    public class RefineryAddOnAnimationComponent : AnimationComponent
    {
        private int kRadius = 120;
        private Effect mTransparentEffect;

        public RefineryAddOnAnimationComponent(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner, bool register = true) : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
           // mTransparentEffect = AssetOps.LoadAsset<Effect>("TransparentEffect");
        }

        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            RefineryAddOn refineryAddOn = Owner as RefineryAddOn;
            PositionComponent positionComponent = refineryAddOn.PositionComponent;

            TileNode tile = TileMap.GetTileForPosition(positionComponent.X, positionComponent.Y);

            if (tile != null && tile.Type == TileType.Placed)
            {
                Layer layer = LayerManager.GetLayerOfEntity(refineryAddOn);
//
              //  spriteBatch.End();
//
              //  spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);
              //  mTransparentEffect.Parameters["Grayscale"].SetValue(true);
              //  mTransparentEffect.CurrentTechnique.Passes[0].Apply();
                spriteBatch.Draw(mTexture, new Vector2(positionComponent.X, positionComponent.Y), CurrentRectangle, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.3f);
              //  spriteBatch.End();

               // spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);
            }
            else
                spriteBatch.Draw(mTexture, positionComponent.Position, CurrentRectangle, Color.White, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.3f);

            if (SystemsManager.Instance.IsPaused)
                return;

            bool isRefining = IsNearbyRefineryWorking_();

            if (!isRefining)
                return;

            CurrentTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(CurrentTime <= 0.0f)
            {
                CurrentTime = Interval;
                CurrentColumnIndex++;
            }

            if(CurrentColumnIndex >= Columns)
            {
                CurrentColumnIndex = 0;
            }

            CurrentRectangle = new Rectangle(SourceRectangle.X + (CurrentColumnIndex * SourceRectangle.Width), SourceRectangle.Y + (CurrentRowIndex * SourceRectangle.Height), SourceRectangle.Width, SourceRectangle.Height);
            
        }


        private bool IsNearbyRefineryWorking_()
        {
            List<int> cells = CollisionHelper.GetCells((Owner as RefineryAddOn).BoundsCollisionComponent);

            foreach (int cell in cells)
            {
                List<CollisionComponent> components = CollisionHelper.GetNearby(cell);
                List<CollisionComponent> refineryComponents = components.Where(c => c.Owner is Refinery).ToList();

                foreach (CollisionComponent component in refineryComponents)
                {
                    Refinery refinery = component.Owner as Refinery;
                    if (Vector2.Distance((Owner as RefineryAddOn).PositionComponent.Origin, refinery.BoundsCollisionComponent.Origin) <= kRadius && refinery.HarvestComponent.IsRefining)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }

}
