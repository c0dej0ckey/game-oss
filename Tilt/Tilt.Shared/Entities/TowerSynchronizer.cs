﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    /*
     * This class is responsible for dealing with buying towers
     * from the tower select menu onto the game. It is also responsible for selling
     * towers as well. The MenuManager is responsible
     * for creating this object, and once a drop is complete, this class
     * removes itself by calling UnRegister. 
     */
    public class TowerSynchronizer : Entity
    {
        private TowerSyncTouchComponent mTouchComponent;
        private ObjectType mObjectType;
        private TowerType mSecondaryType;
        private AddOnType mAddOnType;

        public TowerSynchronizer(ObjectType objectType)
        {
            mObjectType = objectType;

            GraphicsDevice graphicsDevice = ServiceLocator.GetService<GraphicsDevice>();
            Viewport viewport = graphicsDevice.Viewport;

            Rectangle bounds = new Rectangle(0,0, viewport.Width * 2/3, viewport.Height);

            mTouchComponent = new TowerSyncTouchComponent(bounds, this);
            
        }

        public ObjectType Type
        {
            get { return mObjectType; }
            set { mObjectType = value; }
        }

        public TowerType TowerType
        {
            get { return mSecondaryType; }
            set { mSecondaryType = value; }
        }

        public AddOnType AddOnType
        {
            get { return mAddOnType; }
            set { mAddOnType = value; }
        }

        public override void UnRegister()
        {
            mTouchComponent.UnRegister();
            base.UnRegister();
        }


        public TowerSyncTouchComponent TouchComponent
        {
            get { return mTouchComponent; }
            set { mTouchComponent = value; }
        }

    }
}
