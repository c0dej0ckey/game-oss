﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;

namespace Tilt.EntityComponent.Entities
{
    public enum ObjectType
    {
        None,
        Tower,
        Barricade,
        ResourcePile,
        Refinery,
        Base,
        AddOn,
        ShieldGenerator
    }

    public class Barricade 
        : Entity
        , IPlaceable
        , IBuildable
        , ICollideable
        , IHealable
        , IShieldable
    {
        private BarricadeAnimationComponent mAnimationComponent;
        private PositionComponent mPositionComponent;
        private BoundsCollisionComponent mBoundsCollisionComponent;
        private HealthComponent mHealthComponent;
        private HealthRenderComponent mHealthRenderComponent;
        private ShieldRenderComponent mShieldRenderComponent;
        private IData mData;
        private bool mIsEnabled;

        public Barricade(string texturePath, int x, int y, Rectangle sourceRectangle, float interval, int rows, int columns)
        {
            mData = new BarricadeData(10);
            mAnimationComponent = new BarricadeAnimationComponent(texturePath, sourceRectangle, interval, rows, columns, this);
            mPositionComponent = new PositionComponent(x,y,this, new Vector2(x + TileMap.TileWidth / 2, y + TileMap.TileHeight / 2));
            mBoundsCollisionComponent = new BarricadeCollisionComponent(new Rectangle(x,y, mAnimationComponent.Texture.Width, mAnimationComponent.Texture.Height), this);
            mHealthComponent = new ResistantHealthComponent((mData as BarricadeData).Health, this);
            mShieldRenderComponent = new ShieldRenderComponent("lazerbase1", this);
            
        }

        public override void UnRegister()
        {
            mAnimationComponent.UnRegister();
            mPositionComponent.UnRegister();
            mBoundsCollisionComponent.UnRegister();
        }

        public ObjectType ObjectType
        {
            get { return ObjectType.Barricade;}
        }

        public IData Data
        {
            get { return mData; }
            set { mData = value; }
        }

        public BarricadeAnimationComponent RenderComponent
        {
            get { return mAnimationComponent; }
            set { mAnimationComponent = value; }
        }

        public PositionComponent PositionComponent
        {
            get { return mPositionComponent; }
            set { mPositionComponent = value; }
        }

        public BoundsCollisionComponent BoundsCollisionComponent
        {
            get { return mBoundsCollisionComponent; }
            set { mBoundsCollisionComponent = value; }
        }

        public HealthComponent HealthComponent
        {
            get { return mHealthComponent; }
            set { mHealthComponent = value; }
        }

        public HealthRenderComponent HealthRenderComponent
        {
            get { return mHealthRenderComponent; }
            set { mHealthRenderComponent = value; }
        }

        public ShieldRenderComponent ShieldRenderComponent
        {
            get { return mShieldRenderComponent; }
            set { mShieldRenderComponent = value; }
        }

        public bool Enabled
        {
            get { return mIsEnabled; }
            set { mIsEnabled = value; }
        }

    }

    public class BarricadeCollisionComponent : BoundsCollisionComponent
    {
        public BarricadeCollisionComponent(Rectangle bounds, Entity owner) : base(bounds, owner)
        {
        }

        public override void Update()
        {
            Barricade barricade = Owner as Barricade;
            PositionComponent positionComponent = barricade.PositionComponent;

            TileNode tile = TileMap.GetTileForPosition(positionComponent.X, positionComponent.Y);

            if (tile.IsTowerPlaced)
                return;

            foreach (int cell in Cells)
            {
                List<CollisionComponent> nearbyComponents = CollisionHelper.GetNearby(cell);

                foreach (CollisionComponent component in nearbyComponents)
                {
                    if (!(component.Owner is Unit))
                        continue;

                    Unit unit = component.Owner as Unit;
                    UnitPositionComponent unitPosition = unit.PositionComponent;
                    UnitData unitData = unit.Data;

                    HealthComponent healthComponent = barricade.HealthComponent;

                    if (Vector2.Distance(positionComponent.Position, unitPosition.Position) < TileMap.TileWidth)
                    {
                        unit.UnRegister();
                        EventSystem.EnqueueEvent(EventType.UnitDestroyed, unit, null);

                        healthComponent.Health -= unit.Data.Damage;
                    }

                    if (healthComponent.Health <= 0)
                    {
                        EventSystem.EnqueueEvent(EventType.TowerDestroyed, Owner, null);
                    }
                }
            }
        }
    }
}
