﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public class Rocket : Projectile
    {
        private RocketAnimationComponent mAnimationComponent;

        public Rocket(string texturePath, int x, int y, float rotation, ProjectileData projectileData )
            : base(ProjectileType.Rocket)
        {
            PositionComponent = new RocketPositionComponent(x, y, rotation, this);
            CollisionComponent = new RocketCollisionComponent(
                    new Vector2(x,y),
                    new Vector2(x + 46, y),
                    new Vector2(x, y + 8),
                    new Vector2(x + 46, y + 8),
                    new Vector2(x,y),
                    this);
            AnimationComponent = new RocketAnimationComponent(texturePath, new Rectangle(0, 0, 46, 8), 0.0f, 1, 1, this);
            Data = projectileData;
        }

        public RocketAnimationComponent AnimationComponent
        {
            get { return mAnimationComponent; }
            set { mAnimationComponent = value; }
        }

        public override void UnRegister()
        {
            mAnimationComponent.UnRegister();
            PositionComponent.UnRegister();
            CollisionComponent.UnRegister();
            base.UnRegister();
        }
    }

    public class RocketPositionComponent : PositionComponent
    {
        private float mRotation;
        private Vector2 mDirection;
        private Vector2 mLaunchPosition;
        private const int kSpeed = 150;

        public RocketPositionComponent(int x, int y, float rotation, Entity owner)
            : base(x,y, owner)
        {
            mDirection = GeometryOps.Angle2Vector(rotation);
            mRotation = rotation;
            mLaunchPosition = new Vector2(x, y);
        }

        public float Rotation
        {
            get { return mRotation; }
        }

        public Vector2 LaunchPosition
        {
            get { return mLaunchPosition; }
        }

        public override void Update()
        {
            if (SystemsManager.Instance.IsPaused)
                return;

            Rocket rocket = Owner as Rocket;
            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            mPosition += kSpeed * mDirection * (float)gameTime.ElapsedGameTime.TotalSeconds;

            ProjectileData data = rocket.Data;

            if(Vector2.Distance(mLaunchPosition, mPosition) > data.DestructDistance)
            {
                rocket.UnRegister();
            }
        }
    }

    public class RocketCollisionComponent : PointCollisionComponent
    {
        public RocketCollisionComponent(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Vector2 origin, Entity owner)
            : base(p1, p2, p3, p4, owner)
        {
        }

        public override void Update()
        {
            Rocket rocket = Owner as Rocket;
            RocketPositionComponent positionComponent = rocket.PositionComponent as RocketPositionComponent;
            RocketAnimationComponent animationComponent = rocket.AnimationComponent as RocketAnimationComponent;

            if (positionComponent == null)
                return;

            Vector2 position = positionComponent.Position;
            float rotation = positionComponent.Rotation;

            Rectangle sourceRectangle = animationComponent.SourceRectangle;

            var pos1 = new Vector2(position.X, position.Y);
            var pos2 = new Vector2(position.X + sourceRectangle.Width, position.Y);
            var pos3 = new Vector2(position.X, position.Y + sourceRectangle.Height);
            var pos4 = new Vector2(position.X + sourceRectangle.Width, position.Y + sourceRectangle.Height);
            var origin = new Vector2(position.X, position.Y);

            pos1 = GeometryOps.RotateAroundPoint(pos1, origin, positionComponent.Rotation);
            pos2 = GeometryOps.RotateAroundPoint(pos2, origin, positionComponent.Rotation);
            pos3 = GeometryOps.RotateAroundPoint(pos3, origin, positionComponent.Rotation);
            pos4 = GeometryOps.RotateAroundPoint(pos4, origin, positionComponent.Rotation);


            Point1 = new Vector2(pos1.X, pos1.Y);
            Point2 = new Vector2(pos2.X, pos2.Y);
            Point3 = new Vector2(pos3.X, pos3.Y);
            Point4 = new Vector2(pos4.X, pos4.Y);
        }
    }

    public class RocketAnimationComponent : AnimationComponent
    {
        public RocketAnimationComponent(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner)
            : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
        }

        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            Rocket rocket = Owner as Rocket;
            RocketPositionComponent positionComponent = rocket.PositionComponent as RocketPositionComponent;
            Vector2 position = positionComponent.Position;

            spriteBatch.Draw(mTexture, position, CurrentRectangle, Color.White, positionComponent.Rotation, Vector2.Zero, 1.0f, SpriteEffects.None, 0.35f);
        }
    }
}
