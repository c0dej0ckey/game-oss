﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Entities
{
    public class StartMenuPanel : UIElement
    {
        private PanelState mPanelState;
        private StartMenuPanelRenderComponent mRenderComponent;

        public StartMenuPanel(int x, int y, string texturePath, PanelState panelState)
            : base(x,y)
        {
            PositionComponent = new PositionComponent(x, y, this);
            mRenderComponent = new StartMenuPanelRenderComponent(texturePath, this);

        }

        public override void UnRegister()
        {
            PositionComponent.UnRegister();
            mRenderComponent.UnRegister();
            base.UnRegister();
        }
    }

    public class StartMenuPanelRenderComponent : RenderComponent
    {
        public StartMenuPanelRenderComponent(string texturePath, Entity owner)
            : base(texturePath, owner)
        {
        }

        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();

            StartMenuPanel panel = Owner as StartMenuPanel;
            PositionComponent positionComponent = panel.PositionComponent;

            if (panel == null)
                return;

            spriteBatch.Draw(mTexture, positionComponent.Position, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.10f);
            
        }
    }
}
