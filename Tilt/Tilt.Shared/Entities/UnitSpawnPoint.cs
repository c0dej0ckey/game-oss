﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.Shared.Entities
{
    public class UnitSpawnPoint : Entity
    {
        private PositionComponent mPositionComponent;
        private RenderComponent mRenderComponent;

        public UnitSpawnPoint(int x, int y, Rectangle sourceRectangle, int rows, int columns, float interval, string texturePath)
        {
            mPositionComponent = new PositionComponent(x,y, this);
            mRenderComponent = new UnitSpawnPointRenderComponent(texturePath, sourceRectangle, rows, columns, interval, this);
        }

        public PositionComponent PositionComponent
        {
            get { return mPositionComponent; }
            set { mPositionComponent = value; }
        }

        public RenderComponent RenderComponent
        {
            get { return mRenderComponent; }
            set { mRenderComponent = value; }
        }

    }

    public class UnitSpawnPointRenderComponent : AnimationComponent
    {
        private Texture2D mPulseTexture;
        public UnitSpawnPointRenderComponent(string texturePath, Rectangle sourceRectangle, int rows, int columns, float interval, Entity owner, bool register = true) : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
            mPulseTexture = AssetOps.LoadAsset<Texture2D>("pulseanimation");
        }

        public override void Update()
        {
            UnitSpawnPoint unitSpawnPoint = Owner as UnitSpawnPoint;

            PositionComponent positionComponent = unitSpawnPoint.PositionComponent;

            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            spriteBatch.Draw(mPulseTexture, positionComponent.Position - new Vector2(TileMap.TileWidth / 2, TileMap.TileHeight / 2), CurrentRectangle, Color.White,
                0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.65f);

            spriteBatch.Draw(mTexture, positionComponent.Position, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.7f);

            if (SystemsManager.Instance.IsPaused)
                return;

            CurrentTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(CurrentTime <= 0.0f)
            {
                CurrentColumnIndex++;
                CurrentTime = Interval;
            }

            if (CurrentColumnIndex >= Columns)
                CurrentColumnIndex = 0;

            CurrentRectangle = new Rectangle(CurrentColumnIndex * SourceRectangle.Width, CurrentRowIndex * SourceRectangle.Height, SourceRectangle.Width, SourceRectangle.Height);

            

            
        }
    }
}
