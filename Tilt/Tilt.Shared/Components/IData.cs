﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;

namespace Tilt.EntityComponent.Components
{

    public interface IData
    {
    }

    public interface ISellable
    {
        int PriceToBuy { get; set; }

        int PriceToSell { get; set; }
    }

    public interface IFieldOfView
    {
        float FieldOfView { get; set; }
    }

    public class ResourcePileData 
        : IData
    {
        public ResourcePileData(int units, int pricePerUnit)
        {
            Units = units;
            PricePerUnit = pricePerUnit;
        }

        public int Units { get; set; }

        public int PricePerUnit { get; set; }
    }

    public class RefineryData 
        : IData
        , ISellable
    {
        public RefineryData(int unitsPerSec, int health)
        {
            NumUnitsPerSecond = unitsPerSec;
            Health = health;
        }

        ///determines the number of units that can be harvested from a 
        ///resource pile per second.
        public int NumUnitsPerSecond { get; set; }

        public int PriceToBuy { get; set; }
        public int PriceToSell { get; set; }

        public int Health { get; set; }
    }

    public class GoldData : ResourcePileData
    {
        public GoldData(int units, int pricePerUnit) : base(units, pricePerUnit)
        {
        }
        
    }

    public class BarricadeData 
        : IData
        , ISellable
    {
        public BarricadeData(int health)
        {
            Health = health;
        }

        public int PriceToBuy { get; set; }

        public int PriceToSell { get; set; }

        public int Health { get; set; }
    }

    public class AddOnData
        : IData
        , ISellable
        , IFieldOfView
    {
        public AddOnData(float increase, float fieldOfView, int health)
        {
            Increase = increase;
            FieldOfView = fieldOfView;
            Health = health;
        }

        public float FieldOfView { get; set; }
        public float Increase { get; set; }
        public int PriceToBuy { get; set; }
        public int PriceToSell { get; set; }

        public int Health { get; set; }
    }

    public class ShieldGeneratorData
        : IData
        , ISellable
        , IFieldOfView
    {
        public ShieldGeneratorData(int healthBoost, float fieldOfView, int health)
        {
            HealthBoost = healthBoost;
            FieldOfView = fieldOfView;
            Health = health;
        }

        public float FieldOfView { get; set; }
        public int HealthBoost { get; set; }
        public int PriceToBuy { get; set; }
        public int PriceToSell { get; set; }

        public int Health { get; set; }
    }

    public class TowerData 
        : IData
        , ISellable
        , IFieldOfView
    {
        public TowerData(float damage
            , float fieldOfView
            , float fireRate
            , ProjectileType bulletType
            , int priceToBuy
            , int priceToSell
            , int shotsPerFire
            , int health
            , int cooldown
            , int ammoCapacity)
        {
            Damage = damage;
            FieldOfView = fieldOfView;
            FireRate = fireRate;
            BulletType = bulletType;
            PriceToBuy = priceToBuy;
            PriceToSell = priceToSell;
            ShotsPerFire = shotsPerFire;
            FieldOfView = fieldOfView;
            Health = health;
            Cooldown = cooldown;
            AmmoCapacity = ammoCapacity;
        }

        public float Damage { get; set; }
        public float  FieldOfView { get; set; }
        public float FireRate { get; set; }
        public ProjectileType BulletType { get; set; }
        public int PriceToBuy { get; set; }
        public int PriceToSell { get; set; }
        public int ShotsPerFire { get; set; }
        public int Health { get; set; }
        public int AmmoCapacity { get; set; }
        public int Cooldown { get; set; }

    }


    public class ProjectileData
    {

        public ProjectileData(//int damage
             int destructDistance
            , ProjectileType type)
        {
           // Damage = damage;
            DestructDistance = destructDistance;
            Type = type;
        }

        public float Damage { get; set; }
        public int DestructDistance { get; set; }
        public ProjectileType Type { get; set; }
        
    }

    public class UnitData: IData
    {
        public UnitData(int damage, int health)
        {
            Damage = damage;
            Health = health;
        }

        public int Damage { get; set; }

        public int Health { get; set; }
    }

    public class BaseData : IData
    {
    }
}
