﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Structures;
using Tilt.Shared.Utilities;

namespace Tilt.Shared.Components
{
    public class CameraMouseComponent : InputComponent
    {
        private Vector2 mLastPosition;
        private Vector2 mCurrentPosition = Vector2.Zero;

        private Rectangle mLeft;
        private Rectangle mRight;
        private Rectangle mTop;
        private Rectangle mBottom;


        private const int kSpeed = 1200;

        public CameraMouseComponent(Entity owner, bool register = true) : base(owner, register)
        {
            GraphicsDevice graphicsDevice = ServiceLocator.GetService<GraphicsDevice>();
            Viewport viewport = graphicsDevice.Viewport;


            mLeft = new Rectangle(0, 0, 5, viewport.Height);
            mTop = new Rectangle(0, 0, viewport.Width, 5);
            mRight = new Rectangle(viewport.Width - 5, 0, 5, viewport.Height);
            mBottom = new Rectangle(0, viewport.Height - 5, viewport.Width, 5);
        }

        public override void Update()
        {
            Camera camera = Owner as Camera;
            CameraPositionComponent positionComponent = camera.PositionComponent;

            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            //if(MouseOps.IsDrag())
            //{
                
            //    mCurrentPosition = MouseOps.GetPosition();

            //    if (mLastPosition != Vector2.Zero)
            //    {

            //        Vector2 delta = mCurrentPosition - mLastPosition;
            //        Vector2 direction = Vector2.Normalize(delta);

            //        if(float.IsNaN(direction.X) || float.IsNaN(direction.Y))
            //        {
            //        }
            //        else
            //            positionComponent.Position -= direction * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            //    }
                
            //}
            //else
            //{
            //    mCurrentPosition = Vector2.Zero;
            //}
            //mLastPosition = mCurrentPosition;

            mCurrentPosition = MouseOps.GetPosition();

            if(mLeft.Contains(mCurrentPosition))
            {
                positionComponent.X += -1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if(mTop.Contains(mCurrentPosition))
            {
                positionComponent.Y += -1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if(mRight.Contains(mCurrentPosition))
            {
                positionComponent.X += 1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if(mBottom.Contains(mCurrentPosition))
            {
                positionComponent.Y += 1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }


        }
    }

    public class CameraKeyboardComponent : InputComponent
    {
        private const int kSpeed = 1150;
        public CameraKeyboardComponent(Entity owner, bool register = true) : base(owner, register)
        {
        }

        public override  void Update()
        {
            GraphicsDevice graphicsDevice = ServiceLocator.GetService<GraphicsDevice>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            Camera camera = Owner as Camera;
            CameraPositionComponent positionComponent = camera.PositionComponent;
            Viewport viewport = graphicsDevice.Viewport;

            KeyboardState keyboardState = Keyboard.GetState();

            Vector2 cameraSize = new Vector2(viewport.Width / positionComponent.Zoom,
                viewport.Height / positionComponent.Zoom);

            if(keyboardState.IsKeyDown(Keys.W))
            {
                positionComponent.Y += -1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if(keyboardState.IsKeyDown(Keys.S))
            {
                positionComponent.Y += 1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if(keyboardState.IsKeyDown(Keys.A))
            {
                positionComponent.X += -1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if(keyboardState.IsKeyDown(Keys.D))
            {
                positionComponent.X += 1 * kSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            if (positionComponent.X < 0)
                positionComponent.X = 0;
            if (positionComponent.Y < 0)
                positionComponent.Y = 0;
            if(positionComponent.X + cameraSize.X  > Tuner.MapStartPosX + graphicsDevice.Viewport.Width) //FIX THIS!!!!
                positionComponent.X = (Tuner.MapStartPosX + graphicsDevice.Viewport.Width) - cameraSize.X;
            if(positionComponent.Y + cameraSize.Y > Tuner.MapStartPosY/2 + graphicsDevice.Viewport.Height)
                positionComponent.Y = (Tuner.MapStartPosY/2 + graphicsDevice.Viewport.Height) - cameraSize.Y;

            Layer layer = LayerManager.GetLayerOfEntity(Owner);


            layer.Matrix = Matrix.CreateTranslation(new Vector3(-positionComponent.Position, 0)) *
                Matrix.CreateTranslation(new Vector3(-positionComponent.Origin, 0)) *
                Matrix.CreateScale(positionComponent.Zoom, positionComponent.Zoom, 1f) *
                Matrix.CreateTranslation(new Vector3(positionComponent.Origin, 0));


        }
    }
}
