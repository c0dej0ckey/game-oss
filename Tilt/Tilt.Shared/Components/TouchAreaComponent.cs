﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class TouchAreaComponent : InputComponent
    {
        protected Rectangle mBounds;

        public TouchAreaComponent(Rectangle bounds, Entity owner, bool register = true) : base(owner, register) 
        {
            mBounds = bounds;
        }

        public Rectangle Bounds
        {
            get { return mBounds; }
            set { mBounds = value; }
        }

        public override void Update()
        {
            
        }
    }

    public class ButtonTouchComponent : TouchAreaComponent
    {
        public ButtonTouchComponent(Rectangle bounds, Entity owner, bool register = true) : base(bounds, owner, register)
        {
        }

        public override void Update()
        {
            Button button = Owner as Button;
            ButtonAnimationComponent buttonAnimationComponent = button.AnimationComponent;
            AudioComponent audioComponent = button.AudioComponent;

#if !WINDOWS
            if (TouchOps.IsTap() && TouchOps.ContainsPoint(mBounds))
#else 
            if(MouseOps.IsClick() && MouseOps.ContainsPoint(mBounds))
#endif 
            {
                if (!buttonAnimationComponent.IsEnabled || 
                    !buttonAnimationComponent.IsVisible ||
                    buttonAnimationComponent.IsStarted)
                    return;

                buttonAnimationComponent.Start();

                if(audioComponent != null)
                    audioComponent.Play();

#if !WINDOWS
                TouchOps.ClearTouch();
#endif
            }

        }
    }
}
