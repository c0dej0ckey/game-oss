﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class TowerSyncTouchComponent : TouchAreaComponent
    {
        private List<IPlaceable> mPlacedObjects = new List<IPlaceable>();
        private List<IPlaceable> mRemovedObjects = new List<IPlaceable>(); 
        

        public TowerSyncTouchComponent(Rectangle bounds, Entity owner) : base(bounds, owner)
        {
        }

        public List<IPlaceable> PlacedObjects
        {
            get { return mPlacedObjects; }
            set { mPlacedObjects = value; }
        }

        public List<IPlaceable> RemovedObjects
        { 
            get { return mRemovedObjects; }
            set { mRemovedObjects = value; }
        }

        public override void UnRegister()
        {
            mPlacedObjects = null;
            mRemovedObjects = null;
            base.UnRegister();
        }

        public override void Update()
        {
            GameTime gameTime = ServiceLocator.GetService<GameTime>();
            TowerSynchronizer towerSynchronizer = Owner as TowerSynchronizer;
#if WINDOWS
            if (MouseOps.IsClick() &&
                MouseOps.ContainsPoint(mBounds) &&
                SystemsManager.Instance.SelectionMode == SelectionMode.Build)
#else
            if (TouchOps.IsTap() && 
                TouchOps.ContainsPoint(mBounds) &&
                SystemsManager.Instance.SelectionMode == SelectionMode.Build)
#endif
            {
                List<Button> buttons = LayerManager.GetLayer(LayerType.Hud)
                .EntitySystem.Entities.Where(e => e is Button)
                .Cast<Button>().ToList();
#if WINDOWS
                if (buttons.Any(b => b.TouchComponent != null && MouseOps.ContainsPoint(b.TouchComponent.Bounds)))
                    return;
#else
                if(buttons.Any(b => b.TouchComponent != null && TouchOps.ContainsPoint(b.TouchComponent.Bounds)))
                    return;
#endif

                GraphicsDevice viewport = ServiceLocator.GetService<GraphicsDevice>();
                Camera camera = LayerManager.Layer.EntitySystem.GetEntitiesByType<Camera>().FirstOrDefault();
#if WINDOWS
                Vector2 touchLocation = MouseOps.GetPosition();
#else
                Vector2 touchLocation = TouchOps.GetPosition();
#endif
                Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
                Vector2 worldLocation = Vector2.Transform(touchLocation, Matrix.Invert(gameLayer.Matrix));

                Vector2 snappedPosition = new Vector2((float)Math.Floor(worldLocation.X / TileMap.TileHeight) * TileMap.TileWidth, (float)Math.Floor(worldLocation.Y / TileMap.TileHeight) * TileMap.TileHeight);
                TileCoord tileCoord = GeometryOps.PositionToTileCoord(snappedPosition);
                TileNode tileNode = TileMap.GetTileNode(tileCoord.X, tileCoord.Y);

                if (tileNode.Type == TileType.Occupied || tileNode.Type == TileType.Placed ||
                        tileCoord == TileMap.Base && towerSynchronizer.Type != ObjectType.None)
                {
                    EventSystem.EnqueueEvent(EventType.NotificationWindowOpened, this,
                        new NotificationArgs() { Text = "Cannot build there." });
                    return;
                }


                IPlaceable obj = ObjectFactory.Make(towerSynchronizer, (int)snappedPosition.X, (int)snappedPosition.Y);
                if (obj != null)
                {

                    
                    tileNode.Type = TileType.Placed;

                    mPlacedObjects.Add(obj);

                    SlidingPanel slidingPanel =
                        LayerManager.Layer.EntitySystem.GetEntitiesByType<SlidingPanel>().FirstOrDefault();

                    if (mPlacedObjects.Count > 0 &&
                        slidingPanel != null &&
                        slidingPanel.PanelAction == PanelAction.TowerSelected)
                    {
                        slidingPanel.PanelAction = PanelAction.TowerPlaced;

                    }
                }
            }
        }
    }
}

