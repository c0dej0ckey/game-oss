﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;

namespace Tilt.EntityComponent.Components
{

    public class BuffComponent : TimerComponent
    {
        private List<Buff> mBuffs = new List<Buff>();
        public BuffComponent(Entity owner, bool register = true)
            : base(owner, register)
        {
        }

        public override void UnRegister()
        {
            mBuffs.Clear();
        }

        public bool ApplyBuff(ProjectileType projectileType)
        {
            Buff alreadyAppliedBuff = mBuffs.FirstOrDefault(b => b.Type == projectileType);

            if (alreadyAppliedBuff != null)
            {
                alreadyAppliedBuff.Reset();
                return false;
            }

            


            BuffTree.DetermineDeadBuffs(projectileType, mBuffs);
            //BUG: fire towers next to each other with their fires overlapping will instantly kill a creature.
            //This is due to the projectile Id switching every frame. Maybe best to hold a list of projectileId's ?
            //maybe just reset the timer if same tower type?
            Buff buff = BuffFactory.GenerateBuffForEntity(projectileType, Owner);

            if (buff == null)
                return false;

            mBuffs.Add(buff);

            return true;
        }

        public void RemoveBuff(Buff buff)
        {
            mBuffs.Remove(buff);
        }

        public void RemoveBuff(ProjectileType type)
        {
            Buff buff = mBuffs.FirstOrDefault(b => b.Type == type);
            mBuffs.Remove(buff);
        }

        public override void Update()
        {
            foreach (Buff buff in mBuffs.ToList())
            {
                buff.Update();
            }

            base.Update();
        }
    }

}
