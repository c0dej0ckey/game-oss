﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Structures;

namespace Tilt.EntityComponent.Components
{
    public class MapRenderComponent : RenderComponent
    {
        private Texture2D mBackground0;
        private Texture2D mBackground1;
        private Texture2D mBg11;
        private Texture2D mBg12;
        private Texture2D mBg21;
        private Texture2D mBg22;


        public MapRenderComponent(string texturePath, Entity owner) : base(texturePath, owner)
        {
            mBg11 = AssetOps.LoadAsset<Texture2D>("mapbg1-1");
            mBg12 = AssetOps.LoadAsset<Texture2D>("mapbg1-2");
            mBg21 = AssetOps.LoadAsset<Texture2D>("mapbg2-1");
            mBg22 = AssetOps.LoadAsset<Texture2D>("mapbg2-2");
            mBackground0 = AssetOps.LoadAsset<Texture2D>(LevelManager.Level.MapNameLeft);
        }

        public override void Update()
        {

            //in order to show a 2560x1440 map, we have to break the image into 4 tiles
            //and load each one individually. This is a limitation of MonoGame, as the
            //max image size supported is 2048x2048


            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();

            spriteBatch.End();

            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.LinearWrap, null, null);

            Map map = Owner as Map;
            Vector2 topLeft = Vector2.Zero;
            Camera camera = gameLayer.EntitySystem.GetEntitiesByType<Camera>().FirstOrDefault();
            if (camera == null)
                return;

            spriteBatch.Draw(mBg11, topLeft, new Rectangle((int)(camera.PositionComponent.Position.X * 0.5f), (int)(camera.PositionComponent.Position.Y * 0.5f), mBg11.Width, mBg11.Height), Color.White);
            spriteBatch.Draw(mBg21, topLeft, new Rectangle((int)(camera.PositionComponent.Position.X * 0.8f), (int)(camera.PositionComponent.Position.Y * 0.8f), mBg12.Width, mBg12.Height), Color.White);
            
            
            topLeft = new Vector2(mBg11.Width * 1.0f, 0);
            

            spriteBatch.Draw(mBg12, topLeft, new Rectangle((int)(camera.PositionComponent.Position.X * 0.5f), (int)(camera.PositionComponent.Position.Y * 0.5f), mBg21.Width, mBg21.Height), Color.White);
            spriteBatch.Draw(mBg22, topLeft, new Rectangle((int)(camera.PositionComponent.Position.X * 0.8f), (int)(camera.PositionComponent.Position.Y * 0.8f), mBg22.Width, mBg22.Height), Color.White);

            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, gameLayer.Matrix);

            topLeft = new Vector2(Tuner.MapStartPosX, Tuner.MapStartPosY);
            spriteBatch.Draw(mBackground0, topLeft, null, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.1f);
            //topLeft = new Vector2(320 + mBackground1.Width * 1.0f, 192);
           // spriteBatch.Draw(mBackground1, topLeft, null, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.1f);

            

            
        }
    }
}
