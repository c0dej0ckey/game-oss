﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
#define WIN32

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class SelectionBoxTouchComponent : TouchAreaComponent
    {
        public SelectionBoxTouchComponent(Rectangle bounds, Entity owner, bool register = true) : base( bounds, owner, register)
        {
        }

        public override void Update()
        {
#if WINDOWS
            if(MouseOps.IsClick())
            
#else 
            if (TouchOps.IsTap())
#endif
            
            {
#if WINDOWS
                Vector2 touchLocation = MouseOps.GetPosition();
#else
                Vector2 touchLocation = TouchOps.GetPosition();
#endif
                Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
                if (gameLayer == null)
                    return;

                Vector2 worldLocation = Vector2.Transform(touchLocation, Matrix.Invert(gameLayer.Matrix));
                Vector2 snappedPosition = new Vector2((float)Math.Floor(worldLocation.X / TileMap.TileHeight) * TileMap.TileWidth, (float)Math.Floor(worldLocation.Y / TileMap.TileHeight) * TileMap.TileHeight);
                TileCoord tileCoord = GeometryOps.PositionToTileCoord(snappedPosition);
                TileNode tileNode = TileMap.GetTileNode(tileCoord.X, tileCoord.Y);

                if (tileNode == null || tileNode.Object == null)
                    return;

                if (tileNode == TileMap.SelectedTile)
                    return;

                if (SystemsManager.Instance.SelectionMode == SelectionMode.Build && tileNode.Type == TileType.Placed)
                    TileMap.SelectedTile = tileNode;
                if (SystemsManager.Instance.SelectionMode == SelectionMode.Normal && tileNode.Type == TileType.Occupied)
                {
                    TileMap.SelectedTile = tileNode;

                    SelectionBoxAnimationComponent animationComponent = (Owner as SelectionBox).AnimationComponent;
                    animationComponent.CurrentColumnIndex = 0;
                    animationComponent.CurrentTime = animationComponent.Interval;

                    EventSystem.EnqueueEvent(EventType.TowerSelected, this, new TowerSelectedArgs() { Object = TileMap.SelectedTile.Object as Entity });
                    EventSystem.EnqueueEvent(EventType.InfoPanelOpen);
                }
                if (SystemsManager.Instance.SelectionMode == SelectionMode.Sell && tileNode.HasObject)
                    TileMap.SelectedTile = tileNode;
            }

        }


    }
}
