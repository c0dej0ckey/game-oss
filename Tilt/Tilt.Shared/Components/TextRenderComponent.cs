﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;
using Resources = Tilt.EntityComponent.Structures.Resources;
using Tilt.EntityComponent.Structures;

namespace Tilt.EntityComponent.Components
{
    public class TextRenderComponent : Component
    {
        protected string mText;
        private SpriteFont mFont;
        private bool mIsVisible = true;
        private LayerType mRegisteredLayer;

        public TextRenderComponent(string text, string font, Entity owner) : base(owner)
        {
            mText = text;
            mFont = AssetOps.LoadAsset<SpriteFont>(font);
        }

        public string Text
        {
            get { return mText; }
            set { mText = value; }
        }

        public bool IsVisible
        {
            get { return mIsVisible; }
            set { mIsVisible = value; }
        }


        public override void Register()
        {
            mRegisteredLayer = LayerManager.Layer.Type;
            LayerManager.Layer.RenderSystem.Register(this);
        }

        public override void UnRegister()
        {
            LayerManager.GetLayer(mRegisteredLayer).RenderSystem.UnRegister(this);
        }

        public override void Update()
        {
            if (!mIsVisible)
                return;

            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            TextObject textObject = Owner as TextObject;
            PositionComponent positionComponent = textObject.PositionComponent;
            spriteBatch.DrawString(mFont, mText, positionComponent.Position, Color.Red, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.15f);
        }
    }
    
}
