﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class BulletPositionComponent : PositionComponent
    {
        private const int kSpeed = 150;
        private Vector2 mDirection;
        private Vector2 mLaunchPosition;

        public BulletPositionComponent(int x, int y, float rotation, Entity owner) : base(x, y, owner)
        {
            mDirection = GeometryOps.Angle2Vector(rotation);
            mLaunchPosition = new Vector2(x,y);
            Speed = kSpeed;
        }

        public Vector2 Direction
        {
            get { return mDirection;}
        }

        public override void Update()
        {
            if (SystemsManager.Instance.IsPaused)
                return;

            GameTime gameTime = ServiceLocator.GetService<GameTime>();
            Bullet bullet = Owner as Bullet;
            mPosition += mDirection*Speed*(float) gameTime.ElapsedGameTime.TotalSeconds;
            BoundsCollisionComponent collisionComponent = bullet.CollisionComponent as BoundsCollisionComponent;
            Rectangle bulletBounds = collisionComponent.Bounds;
            collisionComponent.Bounds = new Rectangle((int)mPosition.X, (int)mPosition.Y, bulletBounds.Width, bulletBounds.Height );
            ProjectileData projectileData = bullet.Data;
            if (Vector2.Distance(mLaunchPosition, mPosition) > projectileData.DestructDistance)
            {
                bullet.UnRegister();
            }

            //do some logic if the bullet gets too far away from the tower
        }
    }
}
