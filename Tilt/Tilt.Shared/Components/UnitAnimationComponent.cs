﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class UnitAnimationComponent : AnimationComponent
    {
        private Vector2 mLastDirection;
        private EntityState mEntityState;

        public UnitAnimationComponent(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner) 
            : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
            EntityState = EntityState.Walking;
        }

        public EntityState EntityState
        {
            get { return mEntityState; }
            set { mEntityState = value; }
        }

        public override void Update()
        {
            Unit unit = Owner as Unit;
            UnitPositionComponent position = unit.PositionComponent;

            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();

            if (!SystemsManager.Instance.IsPaused)
            {

                if (EntityState == EntityState.Idle)
                {
                    CurrentRowIndex = 0;
                    CurrentColumnIndex = 0;
                    CurrentTime = 0.0f;
                    CurrentRectangle = new Rectangle(CurrentColumnIndex*SourceRectangle.Width,
                        CurrentRowIndex*SourceRectangle.Height, SourceRectangle.Width, SourceRectangle.Height);
                }


                if (position.Direction != mLastDirection)
                {
                    mLastDirection = position.Direction;
                    CurrentRowIndex = position.Direction.Y != 0
                        ? position.Direction.Y == 1 ? 2 : 0
                        : position.Direction.X == 1 ? 3 : 1;
                    CurrentColumnIndex = 0;
                    CurrentTime = 0.0f;
                    CurrentRectangle = new Rectangle(CurrentColumnIndex*SourceRectangle.Width,
                        CurrentRowIndex*SourceRectangle.Height, SourceRectangle.Width, SourceRectangle.Height);
                }

                CurrentTime += (float) gameTime.ElapsedGameTime.TotalSeconds;

                if (CurrentTime > Interval && EntityState == EntityState.Walking)
                {
                    CurrentTime = 0.0f;
                    CurrentColumnIndex = CurrentColumnIndex >= Columns - 1 ? 0 : CurrentColumnIndex + 1;



                    CurrentRectangle = new Rectangle(CurrentColumnIndex*SourceRectangle.Width,
                        CurrentRowIndex*SourceRectangle.Height, SourceRectangle.Width, SourceRectangle.Height);

                }

            }

            spriteBatch.Draw(mTexture, position.Position, CurrentRectangle, Color.White, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.2f);





        }
    }
}
