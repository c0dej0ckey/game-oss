﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;

namespace Tilt.EntityComponent.Components
{
   public class PositionComponent : Component
    {
       private LayerType mRegisteredLayer;
       protected Vector2 mPosition;
       protected Vector2 mOrigin;
       protected int mSpeed;

       public PositionComponent(int x, int y, Entity owner, Vector2 origin = default(Vector2)) : base(owner)
       {
            mPosition = new Vector2(x,y);
            mOrigin = origin;
       }

       public Vector2 Position
       {
           get { return mPosition;;}
           set { mPosition = value; }
       }

       public Vector2 Origin
       {
           get { return mOrigin; }
           set { mOrigin = value; }
       }

       public float X
       {
           get { return mPosition.X; }
           set { mPosition.X = value; }
       }

       public float Y
       {
           get { return mPosition.Y; }
           set { mPosition.Y = value; }
       }

       public int Speed
       {
           get { return mSpeed; }
           set { mSpeed = value; }
       }

       public override void Register()
       {
           mRegisteredLayer = LayerManager.Layer.Type;
           LayerManager.Layer.PositionSystem.Register(this);
       }

       public override void UnRegister()
       {
           LayerManager.GetLayer(mRegisteredLayer).PositionSystem.UnRegister(this);
       }

       public override void Update()
       {
           
       }
    }
}
