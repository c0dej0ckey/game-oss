﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class HealthRenderComponent : RenderComponent
    {
        protected Texture2D mFillTexture;
        public HealthRenderComponent(string borderTexturePath, string fillTexturePath, Entity owner, bool register = true)
            : base(borderTexturePath, owner, register)
        {
            mFillTexture = AssetOps.LoadAsset<Texture2D>(fillTexturePath);
        }
    }

    public class UnitHealthRenderComponent : HealthRenderComponent
    {
        public UnitHealthRenderComponent(string borderTexturePath, string fillTexturePath, Entity owner, bool register = true) : base(borderTexturePath, fillTexturePath, owner, register)
        {
        }
        
        public override void Update()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            Unit unit = Owner as Unit;
            decimal healthPercentage = unit.HealthComponent.HealthPercentage;
            PositionComponent positionComponent = unit.PositionComponent;
            Rectangle healthBarRectangle = new Rectangle(0,0, (int)Math.Ceiling((mFillTexture.Width * healthPercentage)), mFillTexture.Height);

            spriteBatch.Draw(mFillTexture, new Vector2(positionComponent.X, positionComponent.Y - 20),
                healthBarRectangle, Color.White, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.34f);

            spriteBatch.Draw(mTexture, new Vector2(positionComponent.X, positionComponent.Y - 20), null, Color.White, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.35f);

        }

    }
}
