﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.Shared.Components
{
    public class LaserTowerFireState : AnimationState
    {
        private Texture2D mProjectiles;

        private Rectangle mMuzzleRectangle;
        private Rectangle mProjectileSourceRectangle;
        private Rectangle mProjectileCurrentRectangle;

        private float mMuzzleInterval = 0.5f;
        private bool mCharging = true;
        private bool mFired = false;

        public LaserTowerFireState(string towerTexturePath, string projectilesTexturePath, Rectangle towerSourceRectangle, Rectangle projectileSourceRectangle,
            Rectangle muzzleRectangle, float interval, int rows, int columns, Entity owner) 
            : base(towerTexturePath, towerSourceRectangle, interval, rows, columns, owner)
        {
            CurrentColumnIndex = 0;
            CurrentRowIndex = 0;
            mMuzzleRectangle = muzzleRectangle;
            CurrentRectangle = new Rectangle(SourceRectangle.X + (CurrentColumnIndex * TileMap.TileWidth),
                SourceRectangle.Y + (CurrentRowIndex * TileMap.TileHeight), TileMap.TileWidth, TileMap.TileHeight);

            mProjectileSourceRectangle = projectileSourceRectangle;
            mProjectileCurrentRectangle = projectileSourceRectangle;
            mProjectiles = AssetOps.LoadAsset<Texture2D>(projectilesTexturePath);
        }

        public override void Update()
        {
            Tower tower  = Owner as Tower;
            TowerData towerData = tower.Data as TowerData;

            GameTime gameTime = ServiceLocator.GetService<GameTime>();
            LaserTowerAnimationComponent animationComponent = tower.AnimationComponent as LaserTowerAnimationComponent;
            if (animationComponent.TowerState != TowerState.Firing)
                return;

            Draw_();

            if (SystemsManager.Instance.IsPaused)
                return;

            CurrentTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(mCharging)
            {
                if (CurrentTime <= 0.0f)
                {
                    CurrentRowIndex++;
                    CurrentTime = Interval;

                    mProjectileCurrentRectangle = new Rectangle(mProjectileSourceRectangle.X + (CurrentColumnIndex * mProjectileSourceRectangle.Width),
                        mProjectileSourceRectangle.Y + (CurrentRowIndex * mProjectileSourceRectangle.Height), mProjectileSourceRectangle.Width, mProjectileSourceRectangle.Height);

                }

                if(CurrentRowIndex >= Rows)
                {
                    mCharging = false;
                    CurrentRowIndex = Rows - 1;
                    mProjectileCurrentRectangle = new Rectangle(mProjectileSourceRectangle.X + (CurrentColumnIndex * mProjectileSourceRectangle.Width),
                        mProjectileSourceRectangle.Y + (CurrentRowIndex * mProjectileSourceRectangle.Height), mProjectileSourceRectangle.Width, mProjectileSourceRectangle.Height);
                    CurrentTime = mMuzzleInterval;

                    if (!mFired)
                    {
                        Projectile projectile = ProjectileFactory.Make(towerData.BulletType, 0,
                            (int)tower.CannonPositionComponent.X,
                            (int)tower.CannonPositionComponent.Y,
                            tower.CannonPositionComponent.Rotation, tower.Id);

                        if (projectile != null)
                        {
                            ProjectileData projectileData = projectile.Data;
                            projectileData.Damage = towerData.Damage;
                        }

                        tower.AmmoCapacityComponent.Ammo--;
                        mFired = true;
                    }

                }
            }
            else
            {
                if(CurrentTime <= 0.0f)
                {
                    CurrentRowIndex--;
                    CurrentTime = Interval;

                    mProjectileCurrentRectangle = new Rectangle(mProjectileSourceRectangle.X + (CurrentColumnIndex * mProjectileSourceRectangle.Width),
                       mProjectileSourceRectangle.Y + (CurrentRowIndex * mProjectileSourceRectangle.Height), mProjectileSourceRectangle.Width, mProjectileSourceRectangle.Height);
                }

                if(CurrentRowIndex <= 0)
                {
                    animationComponent.TowerState = TowerState.Idle;
                    mCharging = true;
                    mFired = false;
                    CurrentRowIndex = 0;
                    CurrentTime = Interval;
                    mProjectileSourceRectangle = new Rectangle(mProjectileSourceRectangle.X + (CurrentColumnIndex * mProjectileSourceRectangle.Width),
                       mProjectileSourceRectangle.Y + (CurrentRowIndex * mProjectileSourceRectangle.Height), mProjectileSourceRectangle.Width, mProjectileSourceRectangle.Height);
                }
            }

            


        }

        private void Draw_()
        {
            Tower tower = Owner as Tower;
            PositionComponent basePosition = tower.PositionComponent;
            TowerAimerPositionComponent cannonPosition = tower.CannonPositionComponent;

            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();


            spriteBatch.Draw(mTexture, new Vector2(basePosition.X, basePosition.Y), CurrentRectangle, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.3f);


            spriteBatch.Draw(mProjectiles, new Vector2(basePosition.X - 2, basePosition.Y + TileMap.TileHeight / 8), mProjectileCurrentRectangle, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);






        }





    }

    public class LaserTowerIdleState : AnimationState
    {
        private Effect mTransparentEffect;
        public LaserTowerIdleState(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner) : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
            CurrentRowIndex = 0;
            CurrentColumnIndex = 0;
            CurrentRectangle = sourceRectangle;

           // mTransparentEffect = AssetOps.LoadAsset<Effect>("TransparentEffect");
        }
        
        public override void Update()
        {

            Tower tower = Owner as Tower;
            LaserTowerAnimationComponent animationComponent = tower.AnimationComponent as LaserTowerAnimationComponent;
            if (animationComponent.TowerState != TowerState.Idle)
                return;

            Draw();
        }

        private void Draw()
        {
            Tower tower = Owner as Tower;
            PositionComponent basePosition = tower.PositionComponent;
            TowerAimerPositionComponent cannonPosition = tower.CannonPositionComponent;
            CooldownComponent cooldownComponent = tower.CooldownComponent;

            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();

            TileNode tile = TileMap.GetTileForPosition(basePosition.X, basePosition.Y);

            if (tile != null && tile.Type == TileType.Placed)
            {
                Layer layer = LayerManager.GetLayerOfEntity(tower);

               // spriteBatch.End();

                //spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);
               // mTransparentEffect.Parameters["Grayscale"].SetValue(true);
              //  mTransparentEffect.CurrentTechnique.Passes[0].Apply();
                spriteBatch.Draw(mTexture, new Vector2(basePosition.X, basePosition.Y), CurrentRectangle, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.3f);
               // spriteBatch.End();

              //  spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);
            }
            else
                spriteBatch.Draw(mTexture, new Vector2(basePosition.X, basePosition.Y), CurrentRectangle, Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.3f);

        }
    }

    //fix the hierarchy on this -- make members protected in TowerAnimationComponent.
    public class LaserTowerAnimationComponent : TowerAnimationComponentBase
    {
        public LaserTowerAnimationComponent(string towerTexturePath, string projectilesTexturePath, Rectangle towerSourceRectangle, Rectangle projectileSourceRectangle, 
            Rectangle muzzleRectangle, float idleInterval, float fireInterval, int idleRows, int idleColumns, int fireRows, int fireColumns, Entity owner) : 
            base(owner)
        {
            mTowerState = TowerState.Idle;
            mIdleState = new LaserTowerIdleState(towerTexturePath, towerSourceRectangle, idleInterval, idleRows, idleColumns, owner);
            mFireState = new LaserTowerFireState(towerTexturePath, projectilesTexturePath, towerSourceRectangle, projectileSourceRectangle, 
                muzzleRectangle, fireInterval, fireRows, fireColumns, owner);
        }

        public override void Register()
        {
            mRegisteredLayer = LayerManager.Layer.Type;
            LayerManager.Layer.RenderSystem.Register(this);
            base.Register();
        }

        public override void UnRegister()
        {
            LayerManager.GetLayer(mRegisteredLayer).RenderSystem.UnRegister(this);
            mFireState.UnRegister();
            mIdleState.UnRegister();
            base.UnRegister();
        }

        public TowerState TowerState
        {
            get { return mTowerState; }
            set { mTowerState = value; }
        }

        public override void Update()
        {
            if(mTowerState == TowerState.Idle)
            {
                mState = mIdleState;
            }
            if(mTowerState == TowerState.Firing)
            {
                mState = mFireState;
            }

            mState.Update();
        }
    }
}
