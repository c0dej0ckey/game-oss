﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;

namespace Tilt.EntityComponent.Components
{
    public class HealthComponent : Component
    {
        private int mHealth;
        private int mFullHealth;

        public HealthComponent(int health, Entity owner)
            : base(owner, false)
        {
            mHealth = health;
            mFullHealth = health;
        }


        public virtual int Health
        {
            get { return mHealth; }
            set { mHealth = value; }
        }

        public decimal HealthPercentage
        {
            get { return Decimal.Divide(mHealth, mFullHealth); }
        }

    }

    public class ResistantHealthComponent : HealthComponent
    {
        private bool mIsResisting;

        public ResistantHealthComponent(int health, Entity owner)
            : base(health, owner)
        {
        }

        public bool IsResisting
        {
            get { return mIsResisting; }
            set { mIsResisting = value; }
        }

        //resist damage if boosted by shield generators
        public override int Health
        {
            get { return base.Health; }
            set
            {
                int damage = value;

                if (IsResisting)
                    damage--;

                base.Health = damage;
            }
        }
    }
}
