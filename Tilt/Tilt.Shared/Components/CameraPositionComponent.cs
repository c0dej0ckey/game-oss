﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class CameraPositionComponent : PositionComponent
    {
        private Viewport mViewport;
        private Matrix mMatrix;
        private float mZoom;
        private bool mZoomLocked;

        public CameraPositionComponent(int x, int y, Viewport viewport, Entity owner) : base(x, y, owner, new Vector2(viewport.Width / 2, viewport.Height/2))
        {
            mZoom = 1.5f;
            mViewport = viewport;
            mPosition = Vector2.Zero;
            mMatrix = Matrix.CreateTranslation(0,0,0);
        }

        public Matrix Matrix
        {
            get { return mMatrix; }
            set { mMatrix = value; }
        }

        public float Zoom
        {
            get { return mZoom; }
            set { mZoom = value; }
        }

        public bool ZoomLocked
        {
            get { return mZoomLocked; }
            set { mZoomLocked = value; }
        }

        public Viewport Viewport
        {
            get { return mViewport; }
        }

    }
}
