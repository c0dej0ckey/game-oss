﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class RefineryHarvestComponent : TimerComponent
    {
        private const float kOneSecond = 1.0f;
        private const int kRadius = 256;
        private float mTimer = kOneSecond;
        private bool mNoResources;
        private ResourcePile mCurrentResourcePile;



        public RefineryHarvestComponent(Entity owner, bool register = true) : base(owner, register)
        {
        }

        public bool IsTimerDepleted
        {
            get { return mTimer <= 0.0f; }
        }

        public bool IsRefining
        {
            get { return mCurrentResourcePile != null; }
        }

        public override void Update()
        {
            GameTime gameTime = ServiceLocator.GetService<GameTime>();
            Refinery refinery = Owner as Refinery;

            if (refinery == null || !refinery.Enabled || SystemsManager.Instance.IsPaused)
                return;

            PositionComponent refineryPosition = refinery.PositionComponent;
            LoopingAudioComponent audioComponent = refinery.AudioComponent;
            TileCoord refineryTileCoord = GeometryOps.PositionToTileCoord(refineryPosition.Position);

            if (mCurrentResourcePile == null && !mNoResources)
            {
                ResourcePile resourcePile = FindResourcePiles_(refineryTileCoord);
                if (resourcePile != null)
                {
                    mCurrentResourcePile = resourcePile;
                    mTimer = kOneSecond;

                   // if(!audioComponent.IsPlaying)
                      //  audioComponent.Play();
                    
                }
                else
                {
                    //found no resource piles
                    //go idle
                    mNoResources = true;
                    mTimer = kOneSecond;

                    audioComponent.Stop();
                }
            }
            else if (mCurrentResourcePile != null)
            {
                if (IsTimerDepleted)
                {
                    ResourcePileData data = mCurrentResourcePile.Data as ResourcePileData;
                    if (data.Units > 0)
                    {
                        int numberOfAddOns = FindRefineryAddOns_();
                        data.Units--;
                        
                        //secret formula
                        uint price = (uint)( data.PricePerUnit + ((float)numberOfAddOns * 1.5));

                        Resources.Minerals += price;
                    }
                    else
                    {
                        //pile has no more resources. set it to null
                        mCurrentResourcePile.UnRegister();

                        TileCoord tileCoord =
                            GeometryOps.PositionToTileCoord(mCurrentResourcePile.PositionComponent.Position);

                        TileMap.RemoveObjectOnTile(tileCoord.X, tileCoord.Y);
                        mCurrentResourcePile = null;
                    }

                    mTimer = kOneSecond;
                }


                mTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            }
        }

        /// Finds the first resource pile around a given tile coordinate
        private ResourcePile FindResourcePiles_(TileCoord tileCoord)
        {
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int xp = tileCoord.X + i;
                    int yp = tileCoord.Y + j;
                    TileNode tileNode = TileMap.GetTileNode(xp, yp);

                    if (tileNode == null)
                        continue;

                    if (tileNode.Type == TileType.Occupied)
                    {
                        IPlaceable obj = tileNode.Object;
                        if (obj == null)
                            continue;

                        if (obj.ObjectType == ObjectType.ResourcePile)
                        {
                            //check it has some resources
                            ResourcePile resourcePile = obj as ResourcePile;
                            ResourcePileData pileData = resourcePile.Data as ResourcePileData;
                            if (pileData.Units != 0)
                            {
                                return resourcePile;
                            }
                        }
                    }
                }
            }
            return null;
        }

        private int FindRefineryAddOns_()
        {
            int numberOfRefineryAddOnsNearby = 0;

            Refinery refinery = Owner as Refinery;

            List<int> surroundingCells = CollisionHelper.GetSurroundingCells(refinery.BoundsCollisionComponent.Cells.FirstOrDefault());
            foreach (int cell in surroundingCells)
            {
                List<CollisionComponent> nearbyComponents = CollisionHelper.GetNearby(cell);
                List<CollisionComponent> refineryAddOnComponents = nearbyComponents.Where(c => c.Owner is RefineryAddOn).ToList();

                foreach (CollisionComponent collisionComponent in refineryAddOnComponents)
                {
                    RefineryAddOn addOn = collisionComponent.Owner as RefineryAddOn;
                    if (Vector2.Distance(refinery.PositionComponent.Origin, addOn.PositionComponent.Origin) < kRadius)
                    {
                        numberOfRefineryAddOnsNearby++;
                    }
                }
            }

            return numberOfRefineryAddOnsNearby;
        }
    }
}
