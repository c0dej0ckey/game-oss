﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class CameraTouchComponent : InputComponent
    {
        private const float kFlickSpeedScale = 0.5f;
        private const float kKineticSpeedScale = 0.95f;
        private const int kDragSpeed = 120;
        private const float kMaxZoom = 3.0f;
        private const float kMinZoom = 1.0f;
        private Vector2 mDelta;
        private bool mIsFlick;
        private bool mIsZoom;

        private bool mShake;
        private Vector2 mShakeDirection = Vector2.Zero;
        private int mShakeSpeed = 150;
        private const int kShakeSpeed = 150;
        private const float kShakeScale = 0.7f;
        private float mShakeDuration = 0.1f;
        private const float kInterval = 0.1f;
        private int mSign = 1;


        public CameraTouchComponent(Entity owner) : base(owner)
        {
        }

        public bool Shake
        {
            get { return mShake; }
            set
            {
                mShake = value;

                if(value)
                {
                    mShakeDirection = Vector2.Zero;
                    mShakeDuration = kInterval;
                    mShakeSpeed = kShakeSpeed;
                    mSign = 1;
                }
            }
        }

        public override void Update()
        {

            if (LayerManager.Layer.Type == LayerType.GameMenuOverlay)
                return;

            GameTime gameTime = ServiceLocator.GetService<GameTime>();
            GraphicsDevice graphicsDevice = ServiceLocator.GetService<GraphicsDevice>();
            Viewport viewport = graphicsDevice.Viewport;

            Camera camera = Owner as Camera;
            CameraPositionComponent positionComponent = camera.PositionComponent;

            TouchCollection touchCollection = TouchOps.TouchCollection;
            GestureCollection gestureCollection = TouchOps.GestureCollection;

            if (gestureCollection == null)
                return;

            //handle all drag/flick gestures and move the camera
            foreach (TouchLocation location in touchCollection)
            {
                if (location.State == TouchLocationState.Pressed &&
                    mIsFlick &&
                    mDelta != Vector2.Zero)
                {
                    mDelta = Vector2.Zero;
                    mIsFlick = false;
                    mIsZoom = false;
                }
            }

            foreach (GestureSample gesture in gestureCollection)
            {
                switch (gesture.GestureType)
                {
                    case GestureType.Flick:
                    {
                        mDelta = -gesture.Delta*kFlickSpeedScale*(float) gameTime.ElapsedGameTime.TotalSeconds;
                        positionComponent.Position += mDelta;
                        mIsFlick = true;
                        mIsZoom = false;
                    }
                    break;
                    case GestureType.HorizontalDrag:
                    {
                        Vector2 position2= gesture.Position;
                        Vector2 oldPosition = gesture.Position - gesture.Delta;
                        Vector2 delta = position2 - oldPosition;
                        mDelta = new Vector2(delta.X, 0)*kDragSpeed*(float) gameTime.ElapsedGameTime.TotalSeconds;
                        mDelta = -mDelta;

                        positionComponent.Position += mDelta;
                        mIsFlick = false;
                        mIsZoom = false;
                    }
                    break;
                    case GestureType.VerticalDrag:
                    {
                        Vector2 position = gesture.Position;
                        Vector2 oldPosition = gesture.Position - gesture.Delta;
                        Vector2 delta = position - oldPosition;
                        mDelta = new Vector2(0, delta.Y)*kDragSpeed*(float) gameTime.ElapsedGameTime.TotalSeconds;
                        mDelta = -mDelta;
                        positionComponent.Position += mDelta;
                        mIsFlick = false;
                        mIsZoom = false;
                    }
                    break;
                    case GestureType.FreeDrag:
                    {
                        mDelta = -gesture.Delta;
                        positionComponent.Position += mDelta*kDragSpeed*(float) gameTime.ElapsedGameTime.TotalSeconds;
                        mIsFlick = false;
                        mIsZoom = false;
                    }
                    break;
                    case GestureType.Tap:
                    case GestureType.Hold:
                    {
                        mDelta = Vector2.Zero;
                        mIsFlick = false;
                        mIsZoom = false;
                    }
                    break;
                    case GestureType.Pinch:
                    {
                        if (positionComponent.ZoomLocked)
                            break;

                        Vector2 oldPosition = gesture.Position - gesture.Delta;
                        Vector2 oldPosition2 = gesture.Position2 - gesture.Delta2;
                        float distance = Vector2.Distance(gesture.Position, gesture.Position2);
                        float oldDistance = Vector2.Distance(oldPosition, oldPosition2);
                        float zoom;
                        if (distance.Equals(0) || oldDistance.Equals(0))
                            zoom = kMinZoom;
                        else
                            zoom = distance/oldDistance;
                        positionComponent.Zoom *= zoom;
                        if (positionComponent.Zoom > kMaxZoom) positionComponent.Zoom = kMaxZoom;
                        if (positionComponent.Zoom < kMinZoom) positionComponent.Zoom = kMinZoom;
                        mIsFlick = false;
                        mIsZoom = true;
                    }
                    break;
                }
            }

            //kinetic scrolling - gradually slow down flick
            if (mDelta != Vector2.Zero &&
                !TouchPanel.IsGestureAvailable &&
                mIsFlick)
            {
                mDelta = mDelta * kKineticSpeedScale;
                positionComponent.Position += mDelta;

                if ((int)mDelta.X == 0 && (int)mDelta.Y == 0)
                {
                    mDelta = Vector2.Zero;
                    mIsFlick = false;
                }
            }


            // This code below has been moved from CameraPositionComponent.cs. This was moved 
            // in order to support scrolling and building/removing/upgrading towers
            // while paused. This should be in its own class, but I cant find a clean 
            // solution for this

            Vector2 cameraSize = new Vector2(viewport.Width / positionComponent.Zoom,
                viewport.Height / positionComponent.Zoom);

            Layer layer = LayerManager.GetLayerOfEntity(Owner);

            //This code sucks. I have to regenerate the matrix before I can do checking on it
            // otherwise the camera will go OOB, and the frame will be drawn, which looks jittery.
            Vector2 cameraWorldMin = Vector2.Transform(Vector2.Zero,
                Matrix.Invert(
                    Microsoft.Xna.Framework.Matrix.CreateTranslation(new Vector3(-positionComponent.Position, 0)) *
                    Matrix.CreateTranslation(new Vector3(-positionComponent.Origin, 0)) *
                    Matrix.CreateScale(positionComponent.Zoom, positionComponent.Zoom, 1f) *
                    Matrix.CreateTranslation(new Vector3(positionComponent.Origin, 0))));

            Vector2 positionOffset = positionComponent.Position - cameraWorldMin;

            //if (positionComponent.X < positionOffset.X)
            //    positionComponent.X = positionOffset.X;
            //if (positionComponent.Y < positionOffset.Y)
            //    positionComponent.Y = positionOffset.Y;
            //if (positionComponent.X + cameraSize.X + Math.Abs(positionOffset.X) > TileMap.Width)
            //    positionComponent.X = TileMap.Width - cameraSize.X + positionOffset.X;
            //if (positionComponent.Y + cameraSize.Y + Math.Abs(positionOffset.Y) > TileMap.Height)
            //    positionComponent.Y = TileMap.Height - cameraSize.Y + positionOffset.Y;


            if(Shake)
            {
                //generate shake direction
                if (mShakeDirection == Vector2.Zero)
                {
                    Random random = new Random();
                    mShakeDirection = new Vector2((float)(random.NextDouble() * 2.0 - 1.0), (float)(random.NextDouble() * 2.0 - 1.0));
                }

                positionComponent.Position += mShakeDirection * mShakeSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

                mShakeDuration -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if(mShakeDuration <= 0.0f)
                {
                    mShakeDirection = -mShakeDirection;
                    mShakeDuration = kInterval;

                    if (mSign < 0)
                    {
                        mShakeSpeed = (int)(mShakeSpeed * kShakeScale);
                    }

                    mSign = -mSign;
                }

                if(mShakeSpeed <= 10)
                {
                    mShakeDirection = Vector2.Zero;
                    mShakeDuration = kInterval;
                    mShakeSpeed = kShakeSpeed;
                    mShake = false;
                }

            }







            layer.Matrix = Matrix.CreateTranslation(new Vector3(-positionComponent.Position, 0)) *
                Matrix.CreateTranslation(new Vector3(-positionComponent.Origin, 0)) *
                Matrix.CreateScale(positionComponent.Zoom, positionComponent.Zoom, 1f) *
                Matrix.CreateTranslation(new Vector3(positionComponent.Origin, 0));

            if(mDelta != Vector2.Zero || mIsFlick || mIsZoom)
                EventSystem.EnqueueEvent(EventType.MapScrolled);

        }
    }
}
