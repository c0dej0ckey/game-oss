﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class AnimationState : AnimationComponent
    {
        public AnimationState(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner) : base(texturePath, sourceRectangle, interval, rows, columns, owner)
        {
        }
    }

    public class AnimationComponent : RenderComponent
    {
        private float mInterval;
        private int mRows;
        private int mColumns;
        private int mCurrentRowIndex;
        private int mCurrentColumnIndex;
        private Rectangle mSourceRectangle;
        private Rectangle mCurrentRectangle;
        private float mCurrentTime;



        public AnimationComponent(string texturePath, Rectangle sourceRectangle, float interval, int rows, int columns, Entity owner) : base(texturePath, owner)
        {
            mSourceRectangle = sourceRectangle;
            mCurrentRectangle = sourceRectangle;
            mInterval = interval;
            mRows = rows;
            mColumns = columns;
        }

        public float Interval
        {
            get { return mInterval;}
            set { mInterval = value; }
        }

        public Rectangle SourceRectangle
        {
            get { return mSourceRectangle; }
            set { mSourceRectangle = value; }
        }

        public int Rows { get { return mRows; } }

        public int Columns { get { return mColumns; } }

        public float CurrentTime
        {
            get { return mCurrentTime;}
            set { mCurrentTime = value; }
        }

        public int CurrentRowIndex 
        { 
            get { return mCurrentRowIndex; } 
            set { mCurrentRowIndex = value; } 
        }

        public int CurrentColumnIndex
        {
            get { return mCurrentColumnIndex; }
            set { mCurrentColumnIndex = value; }
        }

        public Rectangle CurrentRectangle
        {
            get { return mCurrentRectangle; }
            set { mCurrentRectangle = value; }
        }

        public override void Update()
        {
        }
    }
}
