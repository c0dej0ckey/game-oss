﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class CooldownComponent : TimerComponent
    {
        public CooldownComponent(float timeSet, Entity owner, bool register = true) : base(owner, register)
        {
            mTimeSet = timeSet;
            mTimeLeft = timeSet;
            
            Stop_();
        }

        public bool IsCooling
        {
            get { return mTimeLeft < mTimeSet; }
        }

        public float TimeSet
        {
            get { return mTimeSet; }
            set { mTimeSet = value; }
        }

        public float TimeLeft
        {
            get { return mTimeLeft; }
            set { mTimeLeft = value; }
        }

        public void Cooldown()
        {
            Start_();
        }

        protected override void Done_()
        {
            Reset_();
            Stop_();
        }
    }

    public class CooldownRenderComponent : RenderComponent
    {
        private Texture2D mShadow;

        public CooldownRenderComponent(string texturePath, Entity owner, bool register = true) : base(texturePath, owner, register)
        {
            mShadow = AssetOps.LoadAsset<Texture2D>("fillbarshadow");
        }

        public override void Update()
        {
            Tower tower = Owner as Tower;
            

            if (tower == null)
                return;

            //CooldownComponent cooldownComponent = tower.CooldownComponent;
            //PositionComponent positionComponent = tower.PositionComponent;

            //SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();

            ////only show the cooldown bar if we are cooling
            //if (!cooldownComponent.IsCooling || positionComponent == null)
            //    return;

            //float percentage = (cooldownComponent.IsCooling) ? (cooldownComponent.TimeSet - cooldownComponent.TimeLeft) / cooldownComponent.TimeSet : 1.0f;
            //Rectangle sourceRectangle = new Rectangle(0,0, (int)(mTexture.Width * percentage), (int)(mTexture.Height));

            //spriteBatch.Draw(mShadow, new Vector2(positionComponent.Position.X + TileMap.TileWidth / 2, positionComponent.Position.Y), 
            //    null, Color.White, 0.0f, new Vector2(mTexture.Width / 2, 0), 1.0f, SpriteEffects.None, 0.34f);
            //spriteBatch.Draw(mTexture, new Vector2(positionComponent.Position.X + TileMap.TileWidth / 2, positionComponent.Position.Y), 
            //    sourceRectangle, Color.White, 0.0f, new Vector2(mTexture.Width / 2, 0), 1.0f, SpriteEffects.None, 0.36f);

        }
    }
}
