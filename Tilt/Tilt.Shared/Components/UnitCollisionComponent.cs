﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Components
{
    public class UnitCollisionComponent : BoundsCollisionComponent
    {
        private static readonly int kBulletUnitDistance = (int)(TileMap.TileWidth*2);

        public UnitCollisionComponent(Rectangle bounds, Entity owner) : base(bounds, owner)
        {
        }

        public override void Update()
        {
            PositionComponent unitPosition = (Owner as Unit).PositionComponent;
            mBounds.X = (int) unitPosition.X;
            mBounds.Y = (int) unitPosition.Y;

            foreach (int cell in Cells)
            {
                List<CollisionComponent> nearbyComponents = CollisionHelper.GetNearby(cell);
                foreach (CollisionComponent component in nearbyComponents)
                {
                    if (component == this || !(component.Owner is Projectile))
                        continue;

                    Projectile projectile = component.Owner as Projectile;
                    Unit unit = Owner as Unit;
                    
                    Vector2 unitOrigin = new Vector2(mBounds.X + mBounds.Width / 2, mBounds.Y + mBounds.Height / 2);

                    Vector2 bulletOrigin = projectile.CollisionComponent.Origin;

                    if ((projectile.CollisionComponent is PointCollisionComponent && 
                        GeometryOps.Intersects(((PointCollisionComponent)projectile.CollisionComponent).Points, unit.BoundsCollisionComponent.Bounds)) ||
                        (Vector2.Distance(bulletOrigin, unitOrigin) < kBulletUnitDistance &&
                        projectile.CollisionComponent is BoundsCollisionComponent &&
                        GeometryOps.Intersects(((BoundsCollisionComponent)projectile.CollisionComponent).Bounds, unit.BoundsCollisionComponent.Bounds)))
                    {
                        ////apply buff and take damage
                        if (unit.BuffComponent.ApplyBuff(projectile.ProjectileType))
                        {
                            //possible bug: change health to float now that we have a multiplier
                            unit.HealthComponent.Health -= (int)projectile.Data.Damage;
                        }
                        else
                            unit.HealthComponent.Health -= (int)projectile.Data.Damage;

                        if(!(projectile is Laser))
                            projectile.UnRegister();
                        



                        if (unit.HealthComponent.Health <= 0)
                        {
                            unit.UnRegister();
                            EventSystem.EnqueueEvent(EventType.UnitDestroyed, unit, new UnitDestroyedArgs() { DestroyingEntity = projectile });
                            Resources.UnitsDestroyedOverLevel++;
                        }
                    }

                }
            }
        }
    }
}
