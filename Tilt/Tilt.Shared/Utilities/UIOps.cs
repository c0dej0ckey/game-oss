﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;

namespace Tilt.EntityComponent.Utilities
{
    public class UIOps
    {
        public static UIElement FindElementByName(string name)
        {
            List <UIElement> menuElements =
                LayerManager.Layers.SelectMany(l => l.EntitySystem.Entities).Where(e => e is UIElement).Cast<UIElement>().ToList();
            return menuElements.FirstOrDefault(element => element.Name == name);

        }

        public static void ResetPanelStatePositions(PanelState panelState, Vector2 currentPosition, Vector2 oldPosition)
        {
            foreach (UIElement element in panelState.Elements)
            {
               ResetElementPosition(element, currentPosition, oldPosition);
            }
        }

        public static void ResetElementPosition(UIElement element, Vector2 currentPosition, Vector2 oldPosition)
        {
            Vector2 difference = oldPosition - currentPosition;

             PositionComponent positionComponent = element.PositionComponent;
                positionComponent.Position = positionComponent.Position + difference;

            if (element is Button)
            {
                Button button = element as Button;
                button.TouchComponent.Bounds = new Rectangle((int)positionComponent.X, (int)positionComponent.Y,
                        button.TouchComponent.Bounds.Width, button.TouchComponent.Bounds.Height);
            }
        }

        public static void EnableDisableHudButtons(bool enable)
        {
            Layer hudLayer = LayerManager.GetLayer(LayerType.Hud);

            List<Button> buttons = hudLayer.EntitySystem.GetEntitiesByType<Button>();

            foreach (Button button in buttons)
            {
                button.AnimationComponent.IsEnabled = enable;
            }


        }

        public static Texture2D GetNamePanelSource()
        {
            Layer towerSelectLayer = LayerManager.GetLayer(LayerType.TowerSelect);
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            TowerSynchronizer towerSynchronizer = null;

            ObjectType objType = ObjectType.None;
            TowerType towerType = TowerType.None;
            AddOnType addOnType = AddOnType.None;


            if (towerSelectLayer != null)
            {
                towerSynchronizer = towerSelectLayer.EntitySystem.GetEntitiesByType<TowerSynchronizer>().FirstOrDefault();

                if (towerSynchronizer == null)
                    return null;

                objType = towerSynchronizer.Type;
                towerType = towerSynchronizer.TowerType;
                addOnType = towerSynchronizer.AddOnType;
            }
            else
            {
                if (gameLayer == null)
                    return null;

                TileNode selectedTile = TileMap.SelectedTile;

                if (selectedTile == null)
                    return null;

                IPlaceable placeable = selectedTile.Object;
                objType = placeable.ObjectType;

                if (objType == ObjectType.Tower)
                {
                    Tower tower = placeable as Tower;
                    towerType = tower.Type;
                }
                if (objType == ObjectType.AddOn)
                {
                    AddOn addOn = placeable as AddOn;
                    addOnType = addOn.Type;
                }
            }

            Texture2D selectedTexture = null;
            if(objType == ObjectType.Tower)
            {
                switch(towerType)
                {
                    case TowerType.Bullet:
                        selectedTexture = mBulletPanel;
                        break;
                    case TowerType.Heavy:
                        selectedTexture = mHeavyPanel;
                        break;
                    case TowerType.Shotgun:
                        selectedTexture = mShotgunPanel;
                        break;
                    case TowerType.Laser:
                        selectedTexture = mLaserPanel;
                        break;
                    case TowerType.Nuclear:
                        selectedTexture = mNuclearPanel;
                        break;
                    case TowerType.Rocket:
                        selectedTexture = mRocketPanel;
                        break;
                }
            }

            if(objType == ObjectType.AddOn)
            {
                switch(addOnType)
                {
                    case AddOnType.Cooldown:
                        selectedTexture = mCoolingPanel;
                        break;
                    case AddOnType.AmmoCapacity:
                        selectedTexture = mAmmoAddonPanel;
                        break;
                    case AddOnType.Refinery:
                        selectedTexture = mRefineryAddOnPanel;
                        break;
                    case AddOnType.RangeBooster:
                        selectedTexture = mRangeBoosterAddOnPanel;
                        break;
                }
            }

            if (objType == ObjectType.Barricade)
                selectedTexture = mBarricadePanel;

            if (objType == ObjectType.ShieldGenerator)
                selectedTexture = mShieldGeneratorPanel;

            if (objType == ObjectType.Refinery)
                selectedTexture = mRefineryPanel;

            return selectedTexture;
        
        }

        public static Texture2D GetIconSource()
        {
            Layer towerSelectLayer = LayerManager.GetLayer(LayerType.TowerSelect);
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            TowerSynchronizer towerSynchronizer = null;

            ObjectType objType = ObjectType.None;
            TowerType towerType = TowerType.None;
            AddOnType addOnType = AddOnType.None;


            if (towerSelectLayer != null)
            {
                towerSynchronizer = towerSelectLayer.EntitySystem.GetEntitiesByType<TowerSynchronizer>().FirstOrDefault();

                if (towerSynchronizer == null)
                    return null;

                objType = towerSynchronizer.Type;
                towerType = towerSynchronizer.TowerType;
                addOnType = towerSynchronizer.AddOnType;
            }
            else
            {
                if (gameLayer == null)
                    return null;

                TileNode selectedTile = TileMap.SelectedTile;

                if (selectedTile == null)
                    return null;

                IPlaceable placeable = selectedTile.Object;
                objType = placeable.ObjectType;

                if (objType == ObjectType.Tower)
                {
                    Tower tower = placeable as Tower;
                    towerType = tower.Type;
                }
                if (objType == ObjectType.AddOn)
                {
                    AddOn addOn = placeable as AddOn;
                    addOnType = addOn.Type;
                }
            }

            Texture2D selectedTexture = null;
            if (objType == ObjectType.Tower)
            {
                switch(towerType)
                {
                    case TowerType.Bullet:
                        selectedTexture = mBulletTowerIcon;
                        break;
                    case TowerType.Heavy:
                        selectedTexture = mHeavyTowerIcon;
                        break;
                    case TowerType.Shotgun:
                        selectedTexture = mShotgunTowerIcon;
                        break;
                    case TowerType.Laser:
                        selectedTexture = mLaserTowerIcon;
                        break;
                    case TowerType.Nuclear:
                        selectedTexture = mNuclearTowerIcon;
                        break;
                    case TowerType.Rocket:
                        selectedTexture = mRocketTowerIcon;
                        break;
                }
            }

            if (objType == ObjectType.AddOn)
            {
                switch(addOnType)
                {
                    case AddOnType.Cooldown:
                        selectedTexture = mCoolingAddOnIcon;
                        break;
                    case AddOnType.AmmoCapacity:
                        selectedTexture = mAmmoAddOnIcon;
                        break;
                    case AddOnType.Refinery:
                        selectedTexture = mRefineryAddOnIcon;
                        break;
                    case AddOnType.RangeBooster:
                        selectedTexture = mRangeBoosterAddOnIcon;
                        break;
                }
            }

            if (objType == ObjectType.Barricade)
                selectedTexture = mBarricadeIcon;

            if (objType == ObjectType.ShieldGenerator)
                selectedTexture = mShieldGeneratorIcon;

            if (objType == ObjectType.Refinery)
                selectedTexture = mRefineryIcon;

            return selectedTexture;
        }
    
        public static Texture2D GetPriceSource()
        {
            Layer towerSelectLayer = LayerManager.GetLayer(LayerType.TowerSelect);
            TowerSynchronizer towerSynchronizer = towerSelectLayer.EntitySystem.GetEntitiesByType<TowerSynchronizer>().FirstOrDefault();

            if (towerSynchronizer == null)
                return null;

            Texture2D selectedTexture = null;
            if (towerSynchronizer.Type == ObjectType.Tower)
            {
                switch (towerSynchronizer.TowerType)
                {
                    case TowerType.Bullet:
                        selectedTexture = mBulletTowerPrice;
                        break;
                    case TowerType.Heavy:
                        selectedTexture = mHeavyTowerPrice;
                        break;
                    case TowerType.Shotgun:
                        selectedTexture = mShotgunTowerPrice;
                        break;
                    case TowerType.Laser:
                        selectedTexture = mLaserTowerPrice;
                        break;
                    case TowerType.Nuclear:
                        selectedTexture = mNuclearTowerPrice;
                        break;
                    case TowerType.Rocket:
                        selectedTexture = mRocketTowerPrice;
                        break;
                }
            }

            if (towerSynchronizer.Type == ObjectType.AddOn)
            {
                switch (towerSynchronizer.AddOnType)
                {
                    case AddOnType.Cooldown:
                        selectedTexture = mCoolingAddOnPrice;
                        break;
                    case AddOnType.AmmoCapacity:
                        selectedTexture = mAmmoAddOnPrice;
                        break;
                    case AddOnType.Refinery:
                        selectedTexture = mRefineryAddOnPrice;
                        break;
                    case AddOnType.RangeBooster:
                        selectedTexture = mRangeBoosterAddOnPrice;
                        break;
                }
            }

            if (towerSynchronizer.Type == ObjectType.Barricade)
                selectedTexture = mBarricadePrice;

            if (towerSynchronizer.Type == ObjectType.ShieldGenerator)
                selectedTexture = mShieldGeneratorPrice;

            if (towerSynchronizer.Type == ObjectType.Refinery)
                selectedTexture = mRefineryPrice;

            return selectedTexture;
        }

        public static Texture2D GetDescPanelSource()
        {
            Layer towerSelectLayer = LayerManager.GetLayer(LayerType.TowerSelect);
            TowerSynchronizer towerSynchronizer = towerSelectLayer.EntitySystem.GetEntitiesByType<TowerSynchronizer>().FirstOrDefault();

            if (towerSynchronizer == null)
                return null;

            Texture2D selectedTexture = null;
            if (towerSynchronizer.Type == ObjectType.Tower)
            {
                switch (towerSynchronizer.TowerType)
                {
                    case TowerType.Bullet:
                        selectedTexture = mBulletTowerDescPanel;
                        break;
                    case TowerType.Heavy:
                        selectedTexture = mHeavyTowerDescPanel;
                        break;
                    case TowerType.Shotgun:
                        selectedTexture = mShotgunTowerDescPanel;
                        break;
                    case TowerType.Laser:
                        selectedTexture = mLaserTowerDescPanel;
                        break;
                    case TowerType.Nuclear:
                        selectedTexture = mNuclearTowerDescPanel;
                        break;
                    case TowerType.Rocket:
                        selectedTexture = mRocketTowerDescPanel;
                        break;
                }
            }

            if (towerSynchronizer.Type == ObjectType.AddOn)
            {
                switch (towerSynchronizer.AddOnType)
                {
                    case AddOnType.Cooldown:
                        selectedTexture = mCoolindAddOnDescPanel;
                        break;
                    case AddOnType.AmmoCapacity:
                        selectedTexture = mAmmoAddOnDescPanel;
                        break;
                    case AddOnType.Refinery:
                        selectedTexture = mRefineryAddOnDescPanel;
                        break;
                    case AddOnType.RangeBooster:
                        selectedTexture = mRangeBoosterAddOnDescPanel;
                        break;
                }
            }

            if (towerSynchronizer.Type == ObjectType.Barricade)
                selectedTexture = mBarricadeDescPanel;

            if (towerSynchronizer.Type == ObjectType.ShieldGenerator)
                selectedTexture = mShieldGeneratorDescPanel;

            if (towerSynchronizer.Type == ObjectType.Refinery)
                selectedTexture = mRefineryDescPanel;

            return selectedTexture;
        }

        public static Texture2D GetTextSource()
        {
            Layer towerSelectLayer = LayerManager.GetLayer(LayerType.TowerSelect);
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            TowerSynchronizer towerSynchronizer = null;

            ObjectType objType = ObjectType.None;


            if (towerSelectLayer != null)
            {
                towerSynchronizer = towerSelectLayer.EntitySystem.GetEntitiesByType<TowerSynchronizer>().FirstOrDefault();

                if (towerSynchronizer == null)
                    return null;

                objType = towerSynchronizer.Type;
            }
            else
            {
                if (gameLayer == null)
                    return null;

                TileNode selectedTile = TileMap.SelectedTile;

                if (selectedTile == null)
                    return null;

                IPlaceable placeable = selectedTile.Object;
                objType = placeable.ObjectType;
            }

            Texture2D selectedTexture = null;

            if (objType == ObjectType.Tower)
                selectedTexture = mTowerInfoText;

            if (objType == ObjectType.AddOn)
                selectedTexture = mAddOnInfoText;

            if (objType == ObjectType.Barricade)
                selectedTexture = mBarricadeInfoText;

            if (objType == ObjectType.ShieldGenerator)
                selectedTexture = mShieldGeneratorInfoText;

            if (objType == ObjectType.Refinery)
                selectedTexture = mRefineryInfoText;

            return selectedTexture;
        }

        public static Texture2D GetObjectStatusSource()
        {
            return mOperatingNormalStatus;
        }

        public static Texture2D GetGraphSource()
        {
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            
            if (gameLayer == null)
                return null;

            ObjectType objType = ObjectType.None;
            TowerType towerType = TowerType.None;
            AddOnType addOnType = AddOnType.None;

            TileNode selectedTile = TileMap.SelectedTile;

            if (selectedTile == null)
                return null;

            IPlaceable placeable = selectedTile.Object;
            objType = placeable.ObjectType;

            if (objType == ObjectType.Tower)
            {
                Tower tower = placeable as Tower;
                towerType = tower.Type;
            }
            if (objType == ObjectType.AddOn)
            {
                AddOn addOn = placeable as AddOn;
                addOnType = addOn.Type;
            }
            

            Texture2D selectedTexture = null;
            if (objType == ObjectType.Tower)
            {
                switch (towerType)
                {
                    case TowerType.Bullet:
                        selectedTexture = mEfficiencyGraph5;
                        break;
                    case TowerType.Heavy:
                        selectedTexture = mEfficiencyGraph7;
                        break;
                    case TowerType.Shotgun:
                        selectedTexture = mEfficiencyGraph10;
                        break;
                    case TowerType.Laser:
                        selectedTexture = mEfficiencyGraph3;
                        break;
                    case TowerType.Nuclear:
                        selectedTexture = mEfficiencyGraph4;
                        break;
                    case TowerType.Rocket:
                        selectedTexture = mEfficiencyGraph9;
                        break;
                }
            }

            if (objType == ObjectType.AddOn)
            {
                switch (addOnType)
                {
                    case AddOnType.Cooldown:
                        selectedTexture = mEfficiencyGraph1;
                        break;
                    case AddOnType.AmmoCapacity:
                        selectedTexture = mEfficiencyGraph2;
                        break;
                    case AddOnType.Refinery:
                        selectedTexture = mEfficiencyGraph13;
                        break;
                    case AddOnType.RangeBooster:
                        selectedTexture = mEfficiencyGraph6;
                        break;
                }
            }

            if (objType == ObjectType.Barricade)
                selectedTexture = mEfficiencyGraph8;

            if (objType == ObjectType.ShieldGenerator)
                selectedTexture = mEfficiencyGraph12;

            if (objType == ObjectType.Refinery)
                selectedTexture = mEfficiencyGraph11;

            return selectedTexture;
        
        }



        private static Texture2D mEfficiencyGraph1 = AssetOps.LoadAsset<Texture2D>("efficiencygraph1");
        private static Texture2D mEfficiencyGraph2 = AssetOps.LoadAsset<Texture2D>("efficiencygraph2");
        private static Texture2D mEfficiencyGraph3 = AssetOps.LoadAsset<Texture2D>("efficiencygraph3");
        private static Texture2D mEfficiencyGraph4 = AssetOps.LoadAsset<Texture2D>("efficiencygraph4");
        private static Texture2D mEfficiencyGraph5 = AssetOps.LoadAsset<Texture2D>("efficiencygraph5");
        private static Texture2D mEfficiencyGraph6 = AssetOps.LoadAsset<Texture2D>("efficiencygraph6");
        private static Texture2D mEfficiencyGraph7 = AssetOps.LoadAsset<Texture2D>("efficiencygraph7");
        private static Texture2D mEfficiencyGraph8 = AssetOps.LoadAsset<Texture2D>("efficiencygraph8");
        private static Texture2D mEfficiencyGraph9 = AssetOps.LoadAsset<Texture2D>("efficiencygraph9");
        private static Texture2D mEfficiencyGraph10 = AssetOps.LoadAsset<Texture2D>("efficiencygraph10");
        private static Texture2D mEfficiencyGraph11 = AssetOps.LoadAsset<Texture2D>("efficiencygraph11");
        private static Texture2D mEfficiencyGraph12 = AssetOps.LoadAsset<Texture2D>("efficiencygraph12");
        private static Texture2D mEfficiencyGraph13 = AssetOps.LoadAsset<Texture2D>("efficiencygraph0");

        private static Texture2D mOperatingNormalStatus = AssetOps.LoadAsset<Texture2D>("operatingnormalpanel");
        private static Texture2D mDamagedStatus = AssetOps.LoadAsset<Texture2D>("damagedetectedpanel");
        private static Texture2D mCriticalStatus = AssetOps.LoadAsset<Texture2D>("criticallydamagedpanel");

        private static Texture2D mBulletPanel = AssetOps.LoadAsset<Texture2D>("pistoltowernamepanel");
        private static Texture2D mHeavyPanel = AssetOps.LoadAsset<Texture2D>("heavytowernamepanel");
        private static Texture2D mShotgunPanel = AssetOps.LoadAsset<Texture2D>("shotguntowernamepanel");
        private static Texture2D mLaserPanel = AssetOps.LoadAsset<Texture2D>("lasertowernamepanel");
        private static Texture2D mNuclearPanel = AssetOps.LoadAsset<Texture2D>("nucleartowernamepanel");
        private static Texture2D mCoolingPanel = AssetOps.LoadAsset<Texture2D>("coolingtowernamepanel");
        private static Texture2D mRocketPanel = AssetOps.LoadAsset<Texture2D>("rockettowernamepanel");
        private static Texture2D mAmmoAddonPanel = AssetOps.LoadAsset<Texture2D>("ammoaddonnamepanel");
        private static Texture2D mRefineryAddOnPanel = AssetOps.LoadAsset<Texture2D>("refineryaddonnamepanel");
        private static Texture2D mRangeBoosterAddOnPanel = AssetOps.LoadAsset<Texture2D>("rangeboosternamepanel");
        private static Texture2D mBarricadePanel = AssetOps.LoadAsset<Texture2D>("barricadenamepanel");
        private static Texture2D mShieldGeneratorPanel = AssetOps.LoadAsset<Texture2D>("shieldgeneratornamepanel");
        private static Texture2D mRefineryPanel = AssetOps.LoadAsset<Texture2D>("refinerynamepanel");

        private static Texture2D mBulletTowerIcon = AssetOps.LoadAsset<Texture2D>("pistoltowericon");
        private static Texture2D mHeavyTowerIcon = AssetOps.LoadAsset<Texture2D>("heavytowericon");
        private static Texture2D mShotgunTowerIcon = AssetOps.LoadAsset<Texture2D>("shotguntowericon");
        private static Texture2D mLaserTowerIcon = AssetOps.LoadAsset<Texture2D>("lasertowericon");
        private static Texture2D mNuclearTowerIcon = AssetOps.LoadAsset<Texture2D>("nucleartowericon");
        private static Texture2D mCoolingAddOnIcon = AssetOps.LoadAsset<Texture2D>("coolingtowericon");
        private static Texture2D mRocketTowerIcon = AssetOps.LoadAsset<Texture2D>("rockettowericon");
        private static Texture2D mAmmoAddOnIcon = AssetOps.LoadAsset<Texture2D>("ammoaddonicon");
        private static Texture2D mRefineryAddOnIcon = AssetOps.LoadAsset<Texture2D>("refineryaddonicon");
        private static Texture2D mRangeBoosterAddOnIcon = AssetOps.LoadAsset<Texture2D>("rangeboostericon");
        private static Texture2D mBarricadeIcon = AssetOps.LoadAsset<Texture2D>("barricadeicon");
        private static Texture2D mShieldGeneratorIcon = AssetOps.LoadAsset<Texture2D>("shieldgeneratoricon");
        private static Texture2D mRefineryIcon = AssetOps.LoadAsset<Texture2D>("refineryicon");

        private static Texture2D mBulletTowerPrice = AssetOps.LoadAsset<Texture2D>("pistoltowerpricebar");
        private static Texture2D mHeavyTowerPrice = AssetOps.LoadAsset<Texture2D>("heavytowerpricebar");
        private static Texture2D mShotgunTowerPrice = AssetOps.LoadAsset<Texture2D>("shotguntowerpricebar");
        private static Texture2D mLaserTowerPrice = AssetOps.LoadAsset<Texture2D>("lasertowerpricebar");
        private static Texture2D mNuclearTowerPrice = AssetOps.LoadAsset<Texture2D>("nucleartowerpricebar");
        private static Texture2D mCoolingAddOnPrice = AssetOps.LoadAsset<Texture2D>("coolingtowerpricebar");
        private static Texture2D mRocketTowerPrice = AssetOps.LoadAsset<Texture2D>("rockettowerpricebar");
        private static Texture2D mAmmoAddOnPrice = AssetOps.LoadAsset<Texture2D>("ammoaddonpricebar");
        private static Texture2D mRefineryAddOnPrice = AssetOps.LoadAsset<Texture2D>("refineryaddonpricebar");
        private static Texture2D mRangeBoosterAddOnPrice = AssetOps.LoadAsset<Texture2D>("rangeboosterpricebar");
        private static Texture2D mBarricadePrice = AssetOps.LoadAsset<Texture2D>("barricadepricebar");
        private static Texture2D mShieldGeneratorPrice = AssetOps.LoadAsset<Texture2D>("shieldgeneratorpricebar");
        private static Texture2D mRefineryPrice = AssetOps.LoadAsset<Texture2D>("refinerypricebar");

        private static Texture2D mBulletTowerDescPanel = AssetOps.LoadAsset<Texture2D>("pistoltowerdescpanel");
        private static Texture2D mHeavyTowerDescPanel = AssetOps.LoadAsset<Texture2D>("heavytowerdescpanel");
        private static Texture2D mShotgunTowerDescPanel = AssetOps.LoadAsset<Texture2D>("shotguntowerdescpanel");
        private static Texture2D mLaserTowerDescPanel = AssetOps.LoadAsset<Texture2D>("lasertowerdescpanel");
        private static Texture2D mNuclearTowerDescPanel = AssetOps.LoadAsset<Texture2D>("nucleartowerdescpanel");
        private static Texture2D mCoolindAddOnDescPanel = AssetOps.LoadAsset<Texture2D>("coolingtowerdescpanel");
        private static Texture2D mRocketTowerDescPanel = AssetOps.LoadAsset<Texture2D>("rockettowerdescpanel");
        private static Texture2D mAmmoAddOnDescPanel = AssetOps.LoadAsset<Texture2D>("ammoaddondescpanel");
        private static Texture2D mRefineryAddOnDescPanel = AssetOps.LoadAsset<Texture2D>("refineryaddondescpanel");
        private static Texture2D mRangeBoosterAddOnDescPanel = AssetOps.LoadAsset<Texture2D>("rangeboosterdescpanel");
        private static Texture2D mBarricadeDescPanel = AssetOps.LoadAsset<Texture2D>("barricadedescpanel");
        private static Texture2D mShieldGeneratorDescPanel = AssetOps.LoadAsset<Texture2D>("shieldgeneratordescpanel");
        private static Texture2D mRefineryDescPanel = AssetOps.LoadAsset<Texture2D>("refinerydescpanel");

        private static Texture2D mTowerInfoText = AssetOps.LoadAsset<Texture2D>("towerinfotext");
        private static Texture2D mAddOnInfoText = AssetOps.LoadAsset<Texture2D>("addoninfotext");
        private static Texture2D mShieldGeneratorInfoText = AssetOps.LoadAsset<Texture2D>("shieldgeneratorinfotext");
        private static Texture2D mRefineryInfoText = AssetOps.LoadAsset<Texture2D>("refineryinfotext");
        private static Texture2D mBarricadeInfoText = AssetOps.LoadAsset<Texture2D>("barricadeinfotext");

    }


}
