﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;

namespace Tilt.EntityComponent.Utilities
{
    public static class TouchOps
    {
        private static TouchCollection mTouchCollection;
        private static GestureCollection mGestureCollection;

        public static void Initialize()
        {
            mTouchCollection = new TouchCollection();
            mGestureCollection = new GestureCollection();
        }

        public static void Update()
        {
            mGestureCollection = new GestureCollection();
            mTouchCollection = TouchPanel.GetState();

            while (TouchPanel.IsGestureAvailable)
            {
                GestureSample gesture = TouchPanel.ReadGesture();
                mGestureCollection.Add(gesture);
            }

        }

        public static GestureCollection GestureCollection
        {
            get { return mGestureCollection; }
        }

        public static TouchCollection TouchCollection
        {
            get { return mTouchCollection; }
        }

        public static Vector2 GetPosition()
        {
            return mGestureCollection.FirstOrDefault(g => g.Position != Vector2.Zero).Position;
        }

        public static void ClearTouch()
        {
            mGestureCollection.Clear();
        }

        public static bool ContainsPoint(Rectangle bounds)
        {
            return mGestureCollection.Any(gesture => bounds.Contains(gesture.Position));
        }

        public static bool IsFlick()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.Flick);
        }

        public static bool IsTap()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.Tap);
        }

        public static bool IsDoubleTap()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.DoubleTap);
        }

        public static bool IsHold()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.Hold);
        }

        public static bool IsPinch()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.Pinch);
        }

        public static bool IsPinchComplete()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.PinchComplete);
        }

        public static bool IsFreeDrag()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.FreeDrag);
        }

        public static bool IsHorizontalDrag()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.HorizontalDrag);
        }

        public static bool IsVerticalDrag()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.VerticalDrag);
        }

        public static bool IsDragComplete()
        {
            return mGestureCollection.Any(g => g.GestureType == GestureType.DragComplete);
        }
    }

    public class GestureCollection : Collection<GestureSample>
    {
    }
}
