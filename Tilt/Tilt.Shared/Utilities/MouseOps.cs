﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Tilt.EntityComponent.Utilities;

namespace Tilt.Shared.Utilities
{
    public static class MouseOps
    {
        private static MouseState mPrevMouseState;
        private static MouseState mMouseState;


        public static void Update()
        {
            mPrevMouseState = mMouseState;
            mMouseState = Mouse.GetState();

        }


        public static bool ContainsPoint(Rectangle bounds)
        {
            return bounds.Contains(mMouseState.Position);
        }

        public static Vector2 GetPosition()
        {
            return GeometryOps.PointToVector2(mMouseState.Position);
        }

        public static void SetPosition(Vector2 position)
        {
            Point point = GeometryOps.Vector2ToPoint(position);
            Mouse.SetPosition(point.X, point.Y);
        }

        public static int GetScrollWheelValue()
        {
            return mMouseState.ScrollWheelValue;    
        }

        public static int GetScrollWheelDelta()
        {
            return mMouseState.ScrollWheelValue - mPrevMouseState.ScrollWheelValue;
        }

        public static bool IsMouseScrolling()
        {
            return mMouseState.ScrollWheelValue != mPrevMouseState.ScrollWheelValue;
        }

        public static bool IsClick()
        {
            return mMouseState.LeftButton == ButtonState.Released && mPrevMouseState.LeftButton == ButtonState.Pressed;
        }

        public static bool IsDrag()
        {
            return mMouseState.LeftButton == ButtonState.Pressed && mPrevMouseState.LeftButton == ButtonState.Pressed;
        }
    }
}
