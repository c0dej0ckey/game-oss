﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Structures;

namespace Tilt.EntityComponent.Utilities
{
    public static class AssetOps
    {
        private static Serializer mSerializer;

        static AssetOps()
        {
            mSerializer = new Serializer();
        }

        public static T LoadAsset<T>(string relativePath)
        {
            ContentManager content = ServiceLocator.GetService<ContentManager>();
            T asset = content.Load<T>(String.Format(@"{0}\{1}", Version, relativePath));

            return asset;
        }

        public static Serializer Serializer
        {
            get { return mSerializer; }
        }


        private static string mVersion = "R";

        public static string Version
        {
            get { return mVersion; }
        }

        public static void SetVersion(string model)
        {
            if (model.Contains("iPhone 5"))
                mVersion = "5";
            else if (model == "iPhone 6 Plus")
                mVersion = "P";
            else if (model == "iPhone 6")
                mVersion = "R";
            else if (model == "iPhone 6S Plus")
                mVersion = "P";
            else if (model == "iPhone 6S")
                mVersion = "R";
            else if (model == "iPhone 7 Plus")
                mVersion = "P";
            else if (model == "iPhone 7")
                mVersion = "R";
            else if (model == "iPhone SE")
                mVersion = "SE";
        }
    }
}
