﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Text;

namespace Tilt.Shared.Structures
{
    public static class Tuner
    {
        public static int MapStartPosX = 320;
        public static int MapStartPosY = 192;

        public static int BulletTowerDamage = 1;
        public static int BulletTowerFieldOfView = 256;
        public static int BulletTowerPriceToBuy = 250;
        public static int BulletTowerPriceToSell = 125;
        public static int BulletTowerShotsPerFire = 1;
        public static int BulletTowerHealth = 10;
        public static float BulletTowerFireRate = 2.0f;
        public static int BulletTowerCooldownSeconds = 5;
        public static int BulletTowerAmmoCapacity = 5;

        public static int HeavyTowerDamage = 1;
        public static int HeavyTowerFieldOfView = 256;
        public static int HeavyTowerPriceToBuy = 250;
        public static int HeavyTowerPriceToSell = 125;
        public static int HeavyTowerShotsPerFire = 1;
        public static int HeavyTowerHealth = 10;
        public static float HeavyTowerFireRate = 2.0f;
        public static int HeavyTowerCooldownSeconds = 5;
        public static int HeavyTowerAmmoCapacity = 5;

        public static int LaserTowerDamage = 1;
        public static int LaserTowerFieldOfView = 256;
        public static int LaserTowerPriceToBuy = 250;
        public static int LaserTowerPriceToSell = 125;
        public static int LaserTowerShotsPerFire = 1;
        public static int LaserTowerHealth = 10;
        public static float LaserTowerFireRate = 2.0f;
        public static int LaserTowerCooldownSeconds = 5;
        public static int LaserTowerAmmoCapacity = 5;

        public static int ShotgunTowerDamage = 1;
        public static int ShotgunowerFieldOfView = 256;
        public static int ShotgunTowerPriceToBuy = 250;
        public static int ShotgunTowerPriceToSell = 125;
        public static int ShotgunTowerShotsPerFire = 1;
        public static int ShotgunTowerHealth = 10;
        public static float ShotgunTowerFireRate = 2.0f;
        public static int ShotgunTowerCooldownSeconds = 5;
        public static int ShotgunTowerAmmoCapacity = 5;

        public static int NuclearTowerDamage = 1;
        public static int NuclearTowerFieldOfView = 256;
        public static int NuclearTowerPriceToBuy = 250;
        public static int NuclearTowerPriceToSell = 125;
        public static int NuclearTowerShotsPerFire = 1;
        public static int NuclearTowerHealth = 10;
        public static float NuclearTowerFireRate = 4.0f;
        public static int NuclearTowerCooldownSeconds = 5;
        public static int NuclearTowerAmmoCapacity = 5;

        public static int RocketTowerDamage = 1;
        public static int RocketTowerFieldOfView = 256;
        public static int RocketTowerPriceToBuy = 250;
        public static int RocketTowerPriceToSell = 125;
        public static int RocketTowerShotsPerFire = 1;
        public static int RocketTowerHealth = 10;
        public static float RocketTowerFireRate = 2.0f;
        public static int RocketTowerCooldownSeconds = 5;
        public static int RocketTowerAmmoCapacity = 5;

        public static float AmmoCapacityAddOnIncrease = 1;
        public static int AmmoCapacityAddOnFieldOfView = 256;
        public static int AmmoCapacityAddOnHealth = 10;

        public static float CooldownAddOnIncrease = 0.3f;
        public static int CooldownAddOnFieldOfView = 256;
        public static int CooldownAddOnHealth = 10;

        public static float RefineryAddOnIncrease = 2;
        public static int RefineryAddOnFieldOfView = 256;
        public static int RefineryAddOnHealth = 10;

        public static float RangeBoosterAddOnIncrease = 1.5f;
        public static int RangeBoosterAddOnFieldOfView = 256;
        public static int RangeBoosterAddOnHealth = 10;

        public static int BarricadeHealth = 10;

        public static int RefineryNumUnitsPerSecond = 2;
        public static int RefineryHealth = 10;

        public static int ShieldGeneratorHealthBoost = 2;
        public static int ShieldGeneratorFieldOfView = 256;
        public static int ShieldGeneratorHealth = 10;
    }
}
