﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;

namespace Tilt.EntityComponent.Structures
{

    public class Level
    {
        public List<TileCoord> DeadTiles { get; set; }

        public List<TileCoord> SpawnTiles { get; set; }

        public List<List<UnitType>> Units { get; set; }

        public int Number { get; set; }

        public bool Complete { get; set; }

        public int Time { get; set; }

        public float TimeBetweenSpawns { get; set; }

        public uint Food { get; set; }

        public uint Gold { get; set; }

        public uint Minerals { get; set; }

        public string Name { get; set; }

        public string MapNameLeft { get; set; }

        public string MapNameRight { get; set; }

        public TileCoord Base { get; set; }

        public int BaseHealth { get; set; }

        public List<ResourceTile> ResourceTiles { get; set; } 
    }
}
