﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Utilities;

namespace Tilt.EntityComponent.Structures
{
    public class Mob
    {
        private Queue<UnitType> mQueue;

        public Mob(List<UnitType> mUnits)
        {
            mQueue = new Queue<UnitType>(mUnits);
        }

        public Unit Spawn(TileCoord spawnTile)
        {
            if (IsEmpty())
                return null;

            Vector2 spawn = new Vector2(spawnTile.X * TileMap.TileWidth, spawnTile.Y * TileMap.TileHeight);
            UnitType unitType = mQueue.Dequeue();
            Unit unit = UnitFactory.Make(unitType, (int)spawn.X, (int)spawn.Y, 0.0f, "creatureanimation");

            return unit;
        }

        public bool IsEmpty()
        {
            return mQueue.Count == 0;
        }
    }
}
