﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Systems;

namespace Tilt.EntityComponent.Structures
{
    public static class TileRenderPool
    {
        public static List<TileRenderComponent> mAvailable = new List<TileRenderComponent>();
        public static List<TileRenderComponent> mInUse = new List<TileRenderComponent>();
        private static int kTiles = 1024;

        /// <summary>
        /// Create new TileRenders, and Register them with the Layer if they have not been
        /// </summary>
        public static void Init()
        {
            if (mAvailable.Count < kTiles)
            {
                for (int i = mAvailable.Count; i < kTiles; i++)
                {
                    mAvailable.Add(new TileRenderComponent(null));
                }
            }
            foreach (TileRenderComponent tileRender in mAvailable)
            {
                if(!LayerManager.Layer.RenderSystem.Components.Contains(tileRender))
                    tileRender.Register();
            }

            
        }

        public static TileRenderComponent GetObject()
        {
            if (mAvailable.Count == 0)
            {
                TileRenderComponent tileRenderComponent = new TileRenderComponent(null);
                mInUse.Add(tileRenderComponent);
                tileRenderComponent.IsVisible = true;
                return tileRenderComponent;
            }
            else
            {
                TileRenderComponent tileRenderComponent = mAvailable[0];
                mAvailable.Remove(tileRenderComponent);
                mInUse.Add(tileRenderComponent);
                tileRenderComponent.IsVisible = true;
                return tileRenderComponent;;
            }
        }

        public static void ReleaseObject(TileRenderComponent tileRenderComponent)
        {
            tileRenderComponent.Tile = null;
            tileRenderComponent.IsVisible = false;
            mInUse.Remove(tileRenderComponent);
            mAvailable.Add(tileRenderComponent);
        }
    }
}
