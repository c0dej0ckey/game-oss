﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using Microsoft.Xna.Framework;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Systems;
using Tilt.Shared.Structures;

namespace Tilt.EntityComponent.Structures
{
    public static class ProjectileFactory
    {
        private static Random mRandom = new Random();

        public static Projectile Make(ProjectileType projectileType, int bulletLevel, int x, int y, float rotation, ulong towerId = 0)
        {
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            Layer previousLayer = LayerManager.Layer;

            if (previousLayer != gameLayer)
                LayerManager.Layer = gameLayer;


            Projectile bullet = null;
            switch (projectileType)
            {
                case ProjectileType.Bullet:
                {
                    bullet = new Bullet("projectiles", x, y, new Rectangle(32, 96, 32, 32), rotation, new ProjectileData(450, ProjectileType.Bullet));
                }
                break;
                case ProjectileType.Heavy:
                {
                    bullet = new Bullet("projectiles", x, y, new Rectangle(32, 96, 32, 32), rotation, new ProjectileData(450, ProjectileType.Heavy));
                }
                break;
                case ProjectileType.Shotgun:
                {
                    //generate random offset for the shotgun to similate shotgun spray
                    double rand = mRandom.NextDouble();
                    double randomRotation = rand * (0.52 - -0.52) + -0.52;
                    rotation = rotation + (float)randomRotation;

                    bullet = new Bullet("projectiles", x, y, new Rectangle(32, 96, 32, 32), rotation, new ProjectileData(450, ProjectileType.Bullet));
                }
                break;
                case ProjectileType.Laser:
                {
                    bullet = new Laser("projectile_laser",x,y, new Rectangle(49,14,32,5), 0.0f, 4,1, rotation, new ProjectileData(450, ProjectileType.Laser));
                    
                }
                break;
                case ProjectileType.Sludge:
                {
                    bullet = new Sludge("projectiles",x,y,rotation, new ProjectileData(150, ProjectileType.Sludge));
                }
                break;
                case ProjectileType.Rocket:
                {
                    bullet = new Rocket("rocket", x, y, rotation, new ProjectileData(300, ProjectileType.Rocket));
                }
                break;

            }

            if(previousLayer != gameLayer)
                LayerManager.Layer = previousLayer;

            return bullet;
        }
    }

    public static class UnitFactory
    {
        public static Unit Make(UnitType unitType , int x, int y, float rotation, string texturePath)
        {
            
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);

            Layer previousLayer = LayerManager.Layer;

            if (previousLayer != gameLayer)
                LayerManager.Layer = gameLayer;


            Unit unit = null;
            switch (unitType)
            {
                case UnitType.Basic:
                    unit = new UnitBasic(x,y, TileMap.Base, texturePath, Rectangle.Empty, 20, new UnitData(2, 10));
                    break;
                case UnitType.Heavy:
                    unit = new UnitHeavy(x, y, TileMap.Base, texturePath, Rectangle.Empty, 20, new UnitData(4, 10));
                    break;
                case UnitType.HeavySlow:
                    unit = new UnitHeavySlow(x, y, TileMap.Base, texturePath, Rectangle.Empty, 50, new UnitData(5, 10));
                    break;
                case UnitType.Fast:
                    unit = new UnitFast(x, y, TileMap.Base, texturePath, Rectangle.Empty, 125, new UnitData(3, 10));
                    break;
                case UnitType.LightFast:
                    unit = new UnitLightFast(x, y, TileMap.Base, texturePath, Rectangle.Empty, 150, new UnitData(3, 10));
                    break;
                case UnitType.FastWait:
                    unit = new UnitFastWait(x, y, TileMap.Base, texturePath, Rectangle.Empty, 150, new UnitData(3, 10));
                    break;
            }

            if (previousLayer != gameLayer)
                LayerManager.Layer = previousLayer;

            return unit;
        }

    }

    public static class ObjectFactory
    {
        public static IData GetDataForObject(ObjectType objectType)
        {
            switch (objectType)
            {
                case ObjectType.Barricade:
                    return new BarricadeData(Tuner.BarricadeHealth);
                case ObjectType.Refinery:
                    return new RefineryData(Tuner.RefineryNumUnitsPerSecond
                        , Tuner.RefineryHealth);
                case ObjectType.ShieldGenerator:
                    return new ShieldGeneratorData(Tuner.ShieldGeneratorHealthBoost, 
                        Tuner.ShieldGeneratorFieldOfView, 
                        Tuner.ShieldGeneratorHealth);
            }
            return null;
        }

        public static AddOnData GetDataForAddOn(AddOnType addOnType)
        {
            switch (addOnType)
            {
                case AddOnType.Cooldown:
                    return new AddOnData(Tuner.CooldownAddOnIncrease,
                        Tuner.CooldownAddOnFieldOfView, 
                        Tuner.CooldownAddOnHealth);
                case AddOnType.RangeBooster:
                    return new AddOnData(Tuner.CooldownAddOnIncrease,
                        Tuner.CooldownAddOnFieldOfView,
                        Tuner.CooldownAddOnHealth);
                case AddOnType.Refinery:
                    return new AddOnData(Tuner.RefineryAddOnIncrease,
                    Tuner.RefineryAddOnFieldOfView,
                    Tuner.RefineryAddOnHealth);
                case AddOnType.AmmoCapacity:
                    return new AddOnData(Tuner.AmmoCapacityAddOnIncrease,
                        Tuner.AmmoCapacityAddOnFieldOfView,
                        Tuner.AmmoCapacityAddOnHealth);
            }
            return null;
        }

        public static TowerData GetDataForTower(TowerType towerType)
        {
            switch (towerType)
            {
                case TowerType.Bullet:
                    return new TowerData(Tuner.BulletTowerDamage,
                        Tuner.BulletTowerFieldOfView,
                        Tuner.BulletTowerFireRate,
                        ProjectileType.Bullet, 
                        Tuner.BulletTowerPriceToBuy,
                        Tuner.BulletTowerPriceToSell,
                        Tuner.BulletTowerShotsPerFire,
                        Tuner.BulletTowerHealth,
                        Tuner.BulletTowerCooldownSeconds,
                        Tuner.BulletTowerAmmoCapacity);
                case TowerType.Heavy:
                    return new TowerData(Tuner.HeavyTowerDamage,
                        Tuner.HeavyTowerFieldOfView,
                        Tuner.HeavyTowerFireRate,
                        ProjectileType.Bullet,
                        Tuner.HeavyTowerPriceToBuy,
                        Tuner.HeavyTowerPriceToSell,
                        Tuner.HeavyTowerShotsPerFire,
                        Tuner.HeavyTowerHealth,
                        Tuner.HeavyTowerCooldownSeconds,
                        Tuner.HeavyTowerAmmoCapacity);
                case TowerType.Laser:
                    return new TowerData(Tuner.LaserTowerDamage,
                        Tuner.LaserTowerFieldOfView,
                        Tuner.LaserTowerFireRate,
                        ProjectileType.Laser,
                        Tuner.LaserTowerPriceToBuy,
                        Tuner.LaserTowerPriceToSell,
                        Tuner.LaserTowerShotsPerFire,
                        Tuner.LaserTowerHealth,
                        Tuner.LaserTowerCooldownSeconds,
                        Tuner.LaserTowerAmmoCapacity);
                case TowerType.Nuclear:
                    return new TowerData(Tuner.NuclearTowerDamage,
                        Tuner.NuclearTowerFieldOfView,
                        Tuner.NuclearTowerFireRate,
                        ProjectileType.Sludge,
                        Tuner.NuclearTowerPriceToBuy,
                        Tuner.NuclearTowerPriceToSell,
                        Tuner.NuclearTowerShotsPerFire,
                        Tuner.NuclearTowerHealth,
                        Tuner.NuclearTowerCooldownSeconds,
                        Tuner.NuclearTowerAmmoCapacity);
                case TowerType.Rocket:
                    return new TowerData(Tuner.RocketTowerDamage,
                        Tuner.RocketTowerFieldOfView,
                        Tuner.RocketTowerFireRate,
                        ProjectileType.Rocket,
                        Tuner.RocketTowerPriceToBuy,
                        Tuner.RocketTowerPriceToSell,
                        Tuner.RocketTowerShotsPerFire,
                        Tuner.RocketTowerHealth,
                        Tuner.RocketTowerCooldownSeconds,
                        Tuner.RocketTowerAmmoCapacity);
                case TowerType.Shotgun:
                    return new TowerData(Tuner.ShotgunTowerDamage,
                        Tuner.ShotgunowerFieldOfView,
                        Tuner.ShotgunTowerFireRate,
                        ProjectileType.Shotgun,
                        Tuner.ShotgunTowerPriceToBuy,
                        Tuner.ShotgunTowerPriceToSell,
                        Tuner.ShotgunTowerShotsPerFire,
                        Tuner.ShotgunTowerHealth,
                        Tuner.ShotgunTowerCooldownSeconds,
                        Tuner.ShotgunTowerAmmoCapacity);
            }
            return null;
        }

        public static IPlaceable Make(TowerSynchronizer towerSynchronizer, int x, int y)
        {
            Layer gameLayer = LayerManager.GetLayer(LayerType.Game);
            Layer previousLayer = LayerManager.Layer;

            if (previousLayer != gameLayer)
                LayerManager.Layer = gameLayer;

            IPlaceable obj = null;

            switch (towerSynchronizer.Type)
            {
                case ObjectType.Barricade:
                    obj = new Barricade("buildings", x, y, new Rectangle(0, 32, TileMap.TileWidth, TileMap.TileHeight), 0.0f, 1, 1);
                    break;
                case ObjectType.Refinery:
                    obj = new Refinery("buildings", x, y, new Rectangle(32,32, TileMap.TileWidth, TileMap.TileHeight),  GetDataForObject(ObjectType.Refinery));
                    break;
                case ObjectType.ShieldGenerator:
                    obj = new ShieldGenerator("buildings", x, y, new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 1, 1, GetDataForObject(ObjectType.ShieldGenerator));
                    break;
            }

            if (towerSynchronizer.Type == ObjectType.Tower)
            {

                switch (towerSynchronizer.TowerType)
                {
                    case TowerType.Bullet:
                        obj = new BulletTower(TowerType.Bullet, "towers", "projectiles", x, y, "sfx", GetDataForTower(TowerType.Bullet));
                        break;
                    case TowerType.Heavy:
                        obj = new HeavyTower(TowerType.Heavy, "towers", "projectiles", x, y, "sfx", GetDataForTower(TowerType.Heavy));
                        break;
                    case TowerType.Shotgun:
                        obj = new ShotgunTower(TowerType.Shotgun, "towers", "projectiles", x, y, "sfx", GetDataForTower(TowerType.Shotgun));
                        break;
                    case TowerType.Laser:
                        obj = new LaserTower(TowerType.Laser, "towers", "projectile_laser", x, y, "sfx", GetDataForTower(TowerType.Laser));
                        break;
                    case TowerType.Nuclear:
                        obj = new NuclearTower(TowerType.Nuclear, "towers", "projectiles", x, y, "sfx", GetDataForTower(TowerType.Nuclear));
                        break;
                    case TowerType.Rocket:
                        obj = new RocketTower(TowerType.Rocket, "towers", "projectiles", x, y, "sfx", GetDataForTower(TowerType.Rocket));
                        break;
                }
            }

            if (towerSynchronizer.Type == ObjectType.AddOn)
            {

                switch (towerSynchronizer.AddOnType)
                {
                    case AddOnType.Cooldown:
                        obj = new CooldownAddOn("addons", x, y,
                            new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileWidth), 0.3f, 1, 1,
                            GetDataForAddOn(AddOnType.Cooldown));
                        break;
                    case AddOnType.AmmoCapacity:
                        obj = new AmmoAddOn("buildings_strip6", x, y, 
                            new Rectangle(320, 0, TileMap.TileWidth, TileMap.TileHeight), 0.3f, 1, 2, 
                            GetDataForAddOn(AddOnType.AmmoCapacity));
                        break;
                    case AddOnType.RangeBooster:
                        obj = new RangeBoosterAddOn("buildings_strip6", x, y,
                            new Rectangle(256, 0, TileMap.TileWidth, TileMap.TileHeight), 2.0f, 1, 2,
                            GetDataForAddOn(AddOnType.RangeBooster));
                        break;
                    case AddOnType.Refinery:
                        obj = new RefineryAddOn("buildings_strip6", x, y,
                            new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight), 0.5f, 2, 2,
                            GetDataForAddOn(AddOnType.Refinery));
                        break;
                }
            }

            if (previousLayer != gameLayer)
                LayerManager.Layer = previousLayer;

            return obj;
        }
    }

    public static class BuffFactory
    {
        public static Buff GenerateBuffForEntity(ProjectileType type, Entity entity)
        {
            if (entity is Unit)
            {
                if (type == ProjectileType.Fire)
                {
                    return new UnitFireBuff(1, 5, ProjectileType.Fire, (uint)entity.Id);
                }
            }
            return null;
        }

    }

}
