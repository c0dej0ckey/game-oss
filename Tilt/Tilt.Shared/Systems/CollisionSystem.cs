﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Structures;

namespace Tilt.EntityComponent.Systems
{
    public class CollisionSystem : ISystem
    {
        private List<Component> mComponents = new List<Component>();
        public void Register(Component component)
        {
            mComponents.Add(component);
        }

        public void UnRegister(Component component)
        {
            mComponents.Remove(component);
        }

        public List<Component> Components
        {
            get { return mComponents; }
            set { mComponents = value; }
        }


        public void Update()
        {

            CollisionHelper.ClearCells();
            foreach(CollisionComponent component in mComponents.ToList())
            {
                List<int> cells = CollisionHelper.Register(component);
                component.Cells = cells;
            }

            foreach (CollisionComponent component in mComponents.ToList())
            {
                component.Update();
            }

        }
    }
}
