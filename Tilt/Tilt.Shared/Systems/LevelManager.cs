﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Entities;

namespace Tilt.EntityComponent.Systems
{
    public static class LevelManager
    {
        private static Level mLevel;
        private static List<Level> mLevels;
        private static SaveFile mSaveFile;
        private static Settings mSettings;

        public static void LoadLevels()
        {
            mLevels = AssetOps.Serializer.DeserializeLevelFile("Levels");
            mSaveFile = AssetOps.Serializer.DeserializeSaveFile();
            mLevel = mLevels.ElementAt(SaveFile.LevelCompleted);
            mSettings = AssetOps.Serializer.DeserializeSettings();

        }

        public static void Save()
        {
            Base bse = LayerManager.Layer.EntitySystem.GetEntitiesByType<Base>().FirstOrDefault();

            int baseHealth = (bse != null) ? bse.HealthComponent.Health : Level.BaseHealth;

            uint resources = 0;
            if (mSaveFile.Minerals.TryGetValue(mLevel.Number - 1, ref resources))
            {
                mSaveFile.Minerals[mLevel.Number - 1] = Resources.Minerals;
            }
            else
            {
                mSaveFile.Minerals.Add(Resources.Minerals);
            }


            mSaveFile.LevelCompleted = mLevel.Number;

            mSaveFile.BaseHealth = baseHealth;

            mSaveFile.UnitsDestroyedOverCampaign += Resources.UnitsDestroyedOverLevel;

            mSaveFile.ResourcesSpentOverCampaign += Resources.ResourcesSpentOverLevel;

           AssetOps.Serializer.SerializeSaveFile();
        }

        public static void LoadLevel(int levelNumber)
        {
            if (levelNumber > mLevels.Count || levelNumber < 1)
                levelNumber = 1;

            mLevel = mLevels.ElementAt(levelNumber - 1);
            LoadLevel();

        }

        public static void LoadLevel()
        {
            if (!LayerManager.HasLayer(LayerType.Game))
                LayerManager.Push(LayerType.Game);
            if (!LayerManager.HasLayer(LayerType.Hud))
                LayerManager.Push(LayerType.Hud, true);


            LayerManager.SetLayer(LayerType.Game);


            SystemsManager.Instance.CreateBaseGameObjects();
            
            TileMap.LoadTiles(mLevel.DeadTiles, mLevel.SpawnTiles, mLevel.ResourceTiles);
            TileMap.Base = mLevel.Base;
            Spawner.LoadUnits(mLevel.Units, mLevel.SpawnTiles, mLevel.TimeBetweenSpawns);

            LoadResourcePiles_(mLevel.ResourceTiles);

            if (mLevel.Number == 1)
                Resources.Minerals = mLevel.Minerals;
            else
            {
                //weve already loaded the next leve
                //get the minerals from "2" levels ago
                Resources.Minerals = mSaveFile.Minerals[mLevel.Number - 2];
            }

            Resources.ResourcesSpentOverCampaign = mSaveFile.ResourcesSpentOverCampaign;
            Resources.UnitsDestroyedOverCampaign = mSaveFile.UnitsDestroyedOverCampaign;
            Resources.UnitsDestroyedOverLevel = 0;
            Resources.ResourcesSpentOverLevel = 0;

            InfoBar infoBar = UIOps.FindElementByName("InfoBar") as InfoBar;
            DateTime dateTime = DateTime.Now;

            int hours = mLevel.Time / 3600; //hour of day

            infoBar.SetTimeOfDay(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day,
                hours, 0,0));

            foreach(TileCoord tileCoord in mLevel.SpawnTiles)
            {
                //UnitSpawnPoint spawnPoint = new UnitSpawnPoint(tileCoord.X * TileMap.TileWidth, tileCoord.Y * TileMap.TileHeight, new Rectangle(0,0, 2*TileMap.TileHeight, 2*TileMap.TileWidth), 1, 7, 0.16f, "unitspawnpoint");
            }
        }

        public static Level Level
        {
            get { return mLevel;}
            set { mLevel = value; }
        }

        public static List<Level> Levels
        {
            get { return mLevels;}
        }

        public static SaveFile SaveFile
        {
            get { return mSaveFile; }
            set { mSaveFile = value; }
        }

        public static Settings Settings
        {
            get { return mSettings; }
            set { mSettings = value; }
        }

        //move this somewhere else
        private static void LoadResourcePiles_(List<ResourceTile> resourceTiles)
        {
            foreach (ResourceTile tile in resourceTiles)
            {
                if (tile.ResourceType == ResourceType.Gold)
                {
                    ResourcePile pile = new GoldPile("basicbase2", tile.X * TileMap.TileWidth, tile.Y * TileMap.TileHeight, new GoldData(tile.Amount, 5));
                    TileNode tileNode = TileMap.GetTileNode(tile.X, tile.Y);
                    tileNode.Object = pile;
                }
            }
        }

    }
}
