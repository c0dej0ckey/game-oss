﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Components;

namespace Tilt.EntityComponent.Systems
{
    public class EntitySystem
    {
        private List<Entity> mEntities = new List<Entity>();

        public void Register(Entity entity)
        {
            mEntities.Add(entity);
        }

        public void UnRegister(Entity entity)
        {
            mEntities.Remove(entity);
        }

        public void UnRegisterAll()
        {
            foreach(Entity entity in mEntities.ToList())
                entity.UnRegister();
        }

        public Entity GetEntityById(ulong id)
        {
            return mEntities.FirstOrDefault(e => e.Id == id);
        }

        public List<T> GetEntitiesByType<T>()
        {
            return mEntities.Where(e => e is T).Cast<T>().ToList<T>();
        }

        public List<Entity> Entities
        {
            get { return mEntities; }
            set { mEntities = value; }
        }

    }
}
