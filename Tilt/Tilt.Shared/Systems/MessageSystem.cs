﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;

namespace Tilt.Shared.Systems
{
    public interface IMessage
    {
    }

    public interface IMessageable
    {
        void RecieveMessage(IMessage message);
    }

    public class Message<T> : IMessage
    {
        public T Value {get;set;}

        public ulong FromEntityId {get;set;}

        public ulong ToEntityId {get;set;}
   
    }

    public static class MessageSystem 
    {
        public static void PassMessage<T>(ulong fromEntityId, ulong toEntityId, T value)
        {
            Message<T> message = new Message<T>()
            {
                FromEntityId = fromEntityId,
                ToEntityId = toEntityId,
                Value = value
            };

            //expensive
            Entity entity = LayerManager.Layers.SelectMany(l => l.EntitySystem.Entities).FirstOrDefault(e => e.Id == toEntityId);

            if(entity is IMessageable)
            {
                IMessageable messageable = entity as IMessageable;
                messageable.RecieveMessage(message);
            }
            
        }

        public static void PassMessage<T>(LayerType layerType, ulong fromEntityId, ulong toEntityId, T value)
        {
            Message<T> message = new Message<T>()
            {
                FromEntityId = fromEntityId,
                ToEntityId = toEntityId,
                Value = value
            };

            Layer layer = LayerManager.GetLayer(layerType);

            Entity entity = layer.EntitySystem.GetEntityById(fromEntityId);

            if(entity is IMessageable)
            {
                IMessageable messageable = entity as IMessageable;
                messageable.RecieveMessage(message);
            }

        }

    }
}
