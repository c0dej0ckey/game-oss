﻿// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Utilities;
using Tilt.Shared.Utilities;

namespace Tilt.EntityComponent.Systems
{
    public sealed class SystemsManager
    {
        private static readonly SystemsManager mInstance = new SystemsManager();
        private bool mIsPaused;
        private bool mQuit;
        private List<LayerCaps> mPausedLayerCaps; 
        private SelectionMode mSelectionMode = SelectionMode.Normal;
        private FrameCounter mFrameCounter;

        static SystemsManager(){ }
        private SystemsManager() { }

        public static SystemsManager Instance
        {
            get { return mInstance; }
        }

        public void Initialize()
        {
            GraphicsDevice graphicsDevice = ServiceLocator.GetService<GraphicsDevice>();
            GraphicsDeviceManager deviceManager = ServiceLocator.GetService<GraphicsDeviceManager>();
            
            AudioSystem.Initialize();
            MenuManager.Initialize();
            TouchOps.Initialize();
            
            Layer levelSelect = new Layer(LayerType.LevelSelect);
            Layer hudLayer = new Layer(LayerType.Hud);
            Layer startMenuLayer = new Layer(LayerType.StartMenu);
            Layer levelRecapLayer = new Layer(LayerType.LevelRecap);
            Layer towerSelectLayer = new Layer(LayerType.TowerSelect);
            Layer gameOverLayer = new Layer(LayerType.GameOver);
            Layer menuPanelOverlay = new Layer(LayerType.GameMenuOverlay);
            Layer infoPanelLayer = new Layer(LayerType.Info);

            mFrameCounter = new FrameCounter();


            LayerManager.Push(levelSelect);

            int viewportWidth = deviceManager.PreferredBackBufferWidth;
            int viewportHeight = deviceManager.PreferredBackBufferHeight;

            Button backButton = new NormalButton(0, 0, "menubuttonanimation", new Rectangle(0, 0, 90, 90), 1, 2, null);

            LayerManager.Pop(true);

            LayerManager.Push(hudLayer);



            PauseButton pauseButton = new PauseButton(0, viewportHeight / 16, "playbutton", 1, 2, () => { EventSystem.EnqueueEvent(EventType.PauseButtonPressed, null); }, "sfx", "PauseButton");
            MenuButton settingsButton = new NormalButton(viewportWidth * 90/100,  viewportHeight / 16, "settingsbutton", 1, 1, () => { EventSystem.EnqueueEvent(EventType.PausePanelOpen); });

            pauseButton.PositionComponent.Position = new Vector2(settingsButton.PositionComponent.Position.X - settingsButton.AnimationComponent.SourceRectangle.Width - viewportHeight / 32 , pauseButton.PositionComponent.Y);
            pauseButton.TouchComponent.Bounds = new Rectangle((int)pauseButton.PositionComponent.Position.X, (int)pauseButton.PositionComponent.Position.Y, pauseButton.AnimationComponent.SourceRectangle.Width, pauseButton.AnimationComponent.SourceRectangle.Height);

            MenuButton buildButton =
                new NormalButton(0,0, "buildbutton", new Rectangle(0, 0, 0, 0), 1, 1, MenuManager.OpenBuildPanel);

            buildButton.PositionComponent.Position = new Vector2(viewportWidth - buildButton.AnimationComponent.Texture.Width - viewportWidth / 32, viewportHeight - buildButton.AnimationComponent.Texture.Height - viewportHeight / 32);
            buildButton.AnimationComponent.SourceRectangle = new Rectangle(0, 0, buildButton.AnimationComponent.Texture.Width, buildButton.AnimationComponent.Texture.Height);
            buildButton.TouchComponent.Bounds = new Rectangle((int)buildButton.PositionComponent.X, (int)buildButton.PositionComponent.Y, buildButton.AnimationComponent.Texture.Width, buildButton.AnimationComponent.Texture.Height);

            InfoBar infoBar = new InfoBar("infobar", 0, viewportHeight / 16, "InfoBar");

            LayerManager.Pop(true);

            LayerManager.Push(towerSelectLayer);

            SlidingPanel slidingPanel = new SlidingPanel(viewportWidth, 0, viewportWidth * 2 / 3, 0, "slidingpanelbackground", PanelAction.TowerSelecting);
            TowerSelectList list = new TowerSelectList(viewportWidth, 0, viewportWidth * 2/3, 0, viewportHeight);

            
            NotificationWindow notificationWindow = new NotificationWindow("notification", "NotificationWindowFont", viewportWidth / 4, viewportHeight, viewportWidth / 2, viewportHeight - TileMap.TileHeight);
            (notificationWindow.PositionComponent as NotificationWindowPositionComponent).YDest = viewportHeight - notificationWindow.RenderComponent.Texture.Height;

            

            MenuButton dismissButton = new NormalButton(viewportWidth / 4 + notificationWindow.RenderComponent.Texture.Width - viewportWidth / 16, viewportHeight, "closebutton", 1, 1, () =>
            {
                EventSystem.EnqueueEvent(EventType.NotificationWindowClosed);

            });
            notificationWindow.DismissButton = dismissButton;



            LayerManager.Pop(true);


            LevelManager.LoadLevels();

            LayerManager.Push(startMenuLayer);

            
            int buttonXOffset = viewportWidth / 6;
            int xPadding = viewportWidth / 24;
            int buttonTextureWidth = viewportWidth / 12;
            int buttonTextureHeight = viewportHeight / 6;

            PanelState startState = new PanelState()
            {
                Elements = new List<UIElement>()
                {
                    new NormalButton(viewportWidth * 2 / 3 + xPadding / 4, viewportHeight * 16/100 , "continuegamebutton", 1, 1, MenuManager.OpenWorldMap, "StartMenuContinueGameButton"),
                    new NormalButton(viewportWidth * 2 / 3 + xPadding / 4, viewportHeight * 29/100, "newgamebutton",  1, 1, MenuManager.StartGame, "StartMenuNewGameButton"),
                    new NormalButton(viewportWidth * 2 / 3 + xPadding / 4, viewportHeight * 43/100, "creditsbutton", 1,1, MenuManager.Credits, "StartMenuCreditsButton"),
                    new NormalButton(viewportWidth * 2 / 3 + xPadding / 4, viewportHeight * 56/100, "quitbutton",  1, 1, MenuManager.QuitGame, "StartMenuQuitGameButton"),
                    new MusicButton((viewportWidth * 2/3) + xPadding / 4, viewportHeight * 74/100, "musicbutton", 2,1, () => {EventSystem.EnqueueEvent(EventType.MuteMusic, null, new MuteMusicArgs());}, "StartMenuMuteSoundButton"),
                    new SoundFXButton((viewportWidth * 2/3) + xPadding / 4, viewportHeight * 87/100,"soundfxbutton", 2,1, () => {EventSystem.EnqueueEvent(EventType.MuteSFX, null, new MuteSFXArgs());}, "StartMenuMuteMusicButton")
                    
                }
            };

            StartMenuPanel startMenuPanel = new StartMenuPanel(viewportWidth * 2 / 3, 0, "mainmenubackgroundpanel", startState);

            LayerManager.Pop(true);

            LayerManager.Push(levelRecapLayer);

            RecapPanel victoryRecapPanel = new VictoryRecapPanel("victoryscreenbackgroundpanel", viewportWidth / 3 , viewportHeight / 7);

            NormalButton goToNextLevelButton = new NormalButton(viewportWidth / 2 - xPadding, (viewportHeight * 8 / 10), "continuebutton", 1, 1, () => { MenuManager.OpenWorldMap(); });



            LayerManager.Pop(true);

            LayerManager.Push(gameOverLayer);

            DefeatedRecapPanel defeatedRecapPanel = new DefeatedRecapPanel("defeatscreenbackgroundpanel", viewportWidth / 3, viewportHeight / 7);

            MenuArgButton goToMainMenuButton = new MenuArgButton(viewportWidth * 2/3 - 2*buttonTextureWidth, (viewportHeight * 8 / 10), "quitbutton", 1, 1, new Action<object>(
               (o) =>
               {
                   EventSystem.EnqueueEvent(EventType.StartMenu);
               }),
               LevelManager.Level.Number, true, "DefeatedRecapBackToMainMenuButton");

            LayerManager.Pop(true);

            LayerManager.Push(LayerType.WorldMap);

            WorldMap worldMap = new WorldMap("sfxbutton", 0, 0);
            DialogueTextRenderer textRenderer = new DialogueTextRenderer(xPadding / 2, viewportHeight / 6, "WorldMapTextFont", string.Empty);
            

            Button worldMapBackButton = new MenuArgButton(xPadding / 4, viewportHeight * 72/100, "backbutton", 1, 1, new Action<object>(
               (o) =>
               {
                   EventSystem.EnqueueEvent(EventType.StartMenu);
               }),
               LevelManager.Level.Number, true, "WorldMapBackButton");

            Button worldMapContinueButton = new NormalButton(xPadding / 4, viewportHeight * 86 / 100, "continuebutton", 1, 1, () => { MenuManager.ContinueGame(); }, "WorldMapContinueButton");

            worldMap.Initialize();

            LayerManager.Pop(true);


            LayerManager.Push(infoPanelLayer);

            InfoPanel infoPanel = new InfoPanel(viewportWidth, 0, viewportWidth * 2 / 3, 0, "infobackgroundpanel",  () => { EventSystem.EnqueueEvent(EventType.InfoPanelClose); });
            

            LayerManager.Pop(true);


            LayerManager.Push(menuPanelOverlay);

            PanelState pauseMenuState = new PanelState(PanelAction.PauseMenu)
            {
                Elements = new List<UIElement>()
                {
                    new NormalButton(viewportWidth + xPadding / 4, viewportHeight / 6, "returntogamebutton",  1, 1, () =>
                    {
                        PauseMenuPanel pauseMenuPanel = LayerManager.Layer.EntitySystem.GetEntitiesByType<PauseMenuPanel>().FirstOrDefault();
                        PauseMenuPanelPositionComponent panelPositionComponent = pauseMenuPanel.PositionComponent as PauseMenuPanelPositionComponent;
                        panelPositionComponent.IsSlidingOut = true;
                    }, 
                    "PauseMenuResumeGameButton"),
                    new NormalButton(viewportWidth + xPadding / 4, viewportHeight * 3/10, "mainmenubutton", 1, 1, 
                        () => 
                        { 
                            EventSystem.EnqueueEvent(EventType.StartMenu);
                        }, 
    
                        "PauseMenuGoToStartMenuButton"),
                    new MusicButton(viewportWidth + xPadding / 4, viewportHeight * 57/100 + viewportHeight / 32, "musicbutton", 2, 1, () => {EventSystem.EnqueueEvent(EventType.MuteMusic, null, new MuteMusicArgs());}, "PauseMenuMuteSoundButton"),
                    new SoundFXButton(viewportWidth + xPadding / 4, viewportHeight * 70/100 + viewportHeight / 32,"soundfxbutton", 2, 1, () => {EventSystem.EnqueueEvent(EventType.MuteSFX, null, new MuteSFXArgs());}, "PauseMenuMuteMusicButton")

                }
            };

            PauseMenuPanel pauseMenu = new PauseMenuPanel(viewportWidth, 0, viewportWidth * 2 / 3, 0, "pausemenubackgroundpanel", pauseMenuState, () => { EventSystem.EnqueueEvent(EventType.PausePanelClose); });

            LayerManager.Pop(true);



            LayerManager.Push(LayerType.Game);
            LayerManager.Push(hudLayer);
            LayerManager.SetLayer(LayerType.Game);

            LevelManager.LoadLevel();
        }

        public void CreateBaseGameObjects()
        {
            LayerManager.SetLayer(LayerType.Game);

            GraphicsDevice graphicsDevice = ServiceLocator.GetService<GraphicsDevice>();
            Camera camera = new Camera(0, 0, graphicsDevice.Viewport);
            Map map = new Map(0, 0, "map1");

            int x = LevelManager.Level.Base.X * TileMap.TileWidth;
            int y = LevelManager.Level.Base.Y * TileMap.TileHeight;

            int baseHealth = (LevelManager.Level.Number == 1) ? LevelManager.Level.BaseHealth : LevelManager.SaveFile.BaseHealth;
            Base bse = new Base("buildings_strip6", x,y,
                new Rectangle(64, 0, 2*TileMap.TileWidth, 2*TileMap.TileHeight), 1.0f, 1,3, baseHealth, new BaseData());

            SelectionBox selectionBox = new SelectionBox(Rectangle.Empty, "selectionbox", new Rectangle(0, 0, 36, 36), 0.18f, 1, 1);
            TileMap.SelectedTile = null;
            SelectionMode = SelectionMode.Normal;
        }


        public void Update()
        {
#if WIN32
            MouseOps.Update();
#endif
            TouchOps.Update();
 
            Spawner.Update();
            
            foreach (Layer layer in LayerManager.Layers.ToList())
            {
                layer.Update();
            }
            EventSystem.Update();
        }

        public void Draw()
        {
            SpriteBatch spriteBatch = ServiceLocator.GetService<SpriteBatch>();
            GameTime gameTime = ServiceLocator.GetService<GameTime>();
            for (int i = LayerManager.Layers.Count - 1; i >= 0; i--)
            {
                Layer layer = LayerManager.Layers.ElementAt(i);

                spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, layer.Matrix);

                if (layer.Type == LayerType.Hud)
                {
                    mFrameCounter.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
                    var fps = string.Format("FPS: {0}", mFrameCounter.AverageFramesPerSecond);

                    //spriteBatch.DrawString(mFont, fps, new Vector2(0, 20), Color.White);
                }

                layer.Draw();
                spriteBatch.End();
            }
        }

        public bool IsPaused
        {
            get { return mIsPaused; }
            set
            {
                if (mIsPaused != value)
                {

                    mIsPaused = value;
                    if (mIsPaused)
                    {
                        mPausedLayerCaps = LayerManager.Layers.Select(l => l.Caps).ToList();
                        foreach (Layer layer in LayerManager.Layers.ToList())
                        {
                            if (LayerManager.Layers.ToList().IndexOf(layer) != 0)
                                layer.Caps = LayerCaps.Render | LayerCaps.Touch | LayerCaps.Collision | LayerCaps.Position;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < LayerManager.Layers.Count; i++)
                        {
                            Layer layer = LayerManager.Layers.ElementAt(i);
                            layer.Caps = LayerCaps.All;
                        }

                    }
                }
                EventSystem.EnqueueEvent(EventType.PauseChanged, this, null);
            }
        }

        public SelectionMode SelectionMode
        {
            get { return mSelectionMode; }
            set
            {
                mSelectionMode = value;
                EventSystem.EnqueueEvent(EventType.SelectionModeChanged, mSelectionMode, null);
            }
        }

        public bool Quit
        {
            get { return mQuit; }
            set { mQuit = value; }
        }

    }
}
