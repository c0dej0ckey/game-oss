// Copyright 2017 - onwards Scott Williams 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
// IN THE SOFTWARE.
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.CompilerServices;
using Android.Content.Res;
using Java.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Tilt.EntityComponent.Components;
using Tilt.EntityComponent.Entities;
using Tilt.EntityComponent.Structures;
using Tilt.EntityComponent.Systems;
using Tilt.EntityComponent.Utilities;

namespace Tilt.TD
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private FrameCounter frameCounter;
        private SpriteFont mFont;
        private GestureSample mGesture;
        private Vector2 mDelta;
        private bool mIsFlick;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = true;
            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft;
        }

        protected override void Initialize()
        {
            TouchPanel.EnabledGestures = GestureType.None | GestureType.Hold | GestureType.Flick | GestureType.FreeDrag | 
                GestureType.Tap | GestureType.DoubleTap | GestureType.Pinch | GestureType.PinchComplete | 
                GestureType.DragComplete | GestureType.HorizontalDrag | GestureType.VerticalDrag | GestureType.DoubleTap;
            TouchPanelCapabilities caps = TouchPanel.GetCapabilities();

            base.Initialize();
        }

       
        protected override void LoadContent()
        {
            graphics.PreferredBackBufferWidth =  GraphicsDevice.DisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            


            ServiceLocator.AddService<GraphicsDevice>(GraphicsDevice);
            ServiceLocator.AddService<GraphicsDeviceManager>(graphics);
            ServiceLocator.AddService<SpriteBatch>(spriteBatch);
            ServiceLocator.AddService<ContentManager>(Content);


            frameCounter = new FrameCounter();

            mFont = AssetOps.LoadAsset<SpriteFont>("DebugFont");

            AssetOps.Serializer.DeserializeStringsFile("thisisatest");
            
            SystemsManager.Instance.Initialize();
        }

     
        protected override void UnloadContent()
        {
        }

       
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || SystemsManager.Instance.Quit)
                Quit_();

            ServiceLocator.RemoveService<GameTime>();
            ServiceLocator.AddService<GameTime>(gameTime);

            SystemsManager.Instance.Update();


            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(51,51,51));

            SystemsManager.Instance.Draw();
            
            base.Draw(gameTime);
        }

        private void Quit_()
        {
            //for (int i = 0; i < LayerManager.Layers.Count; i++)
            //{
            //    Layer layer = LayerManager.Layers.ElementAt(i);
            //    layer.EntitySystem.UnRegisterAll();
            //    LayerManager.Pop();
            //}
            Exit();
        }

    }
}
